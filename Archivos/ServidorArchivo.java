import java.net.*;
import java.io.*;
public class ServidorArchivo {
    public static void main(String[] args) {
        try{
            ServerSocket s = new ServerSocket(7000);
            //INICIAMOS CICLO ESPRANDO CONEXIÓN
            for(;;){
                Socket cl = s.accept();
                System.out.println("Conexión establecida desde"+cl.getInetAddress()+":"+cl.getPort());
                DataInputStream dis = new DataInputStream(cl.getInputStream());
                //LEMOS ARCHIVO Y CREAMOS FLUJO PAR ESCRIBIR EL ARCHIVO DE SALIDA
                byte[] b = new byte[1024];
                String nombre = dis.readUTF();
                System.out.println("Recibimos el archivo: "+nombre);
                long tam = dis.readLong();
                DataOutputStream dos = new DataOutputStream(new FileOutputStream(nombre));
                //PREPARAMOS DATOS PARA RECIBIR PAQUETES DEL ARCHIVO
                long recibidos = 0;
                int n, porcentaje;
                //RECIBIMOS DATOS ENVIADOS POR EL CLIENTE
                while(recibidos < tam){
                    n = dis.read(b);
                    dos.write(b,0,n);
                    dos.flush();
                    recibidos = recibidos + n;
                    porcentaje = (int)(recibidos*100/tam);
                    System.out.print("\n\nArchivo recibido.");
                }
                dos.close();
                dis.close();
                cl.close();
            }      
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
