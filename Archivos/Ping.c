#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <sys/types.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <asm/types.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h> //For errno - the error number
#include <netdb.h> //hostent
//variables globales
typedef struct sred
{
	int indice,paquete;
	unsigned char ip[4];
	unsigned char mac[6];
	unsigned char mascara[4];
	unsigned char ipenlace[4];
	unsigned char macenlace[6];
	unsigned char ippu[4];
}sred;
unsigned char tuip[4],tumac[6];
sred *cabr=NULL;
FILE *puerta;
int i;
int i,ctapac[3]={0,0,0},intime=0;
unsigned char ethertypeping[2]={0x08,0x00};
unsigned char tramapingenv[1514];
unsigned char tramapingrec[1514];
//Varibales ARP//
unsigned char etertype[2]={0x08,0x06};
unsigned char tramaarpenv[48];
unsigned char tramaarprec[48];
unsigned char codenvio[2]={0x00,0x01};
unsigned char codrespuesta[2]={0x00,0x02};
////Declaración de funciones///
void parametros(char *dest,int *ttl,int *pac,unsigned char *tuip,char *mensaje);
void putip(unsigned char *ip);
void putmac(unsigned char *mac);
void putdatos(sred *cabr);
sred * datosred(char *interfaz,int paq);
char* hostname_to_ip(char * hostname);
void estadisticas(int *tiempopac,unsigned char *tuip,int pac);
int compruebared(sred *cabr,unsigned char *tuip);
void checksum(int limi, int lims, unsigned char *check0, unsigned char *check1, unsigned char *mtrama);
int reciveping(sred *cabr,unsigned char *tuip,int *tiempopac,int sec,int tamtra);
void enviaping(sred *cabr,unsigned char *tuip,int *tiempopac,int sec,int tamtra);
void estructuraping(sred*cabr,unsigned char *tumac,unsigned char *tuip,int secuencia,int ttl,int red,int *tiempopac,char *mensaje);
int resivearp(sred *cabr,unsigned char *tuip,unsigned char *tumac);
void enviaarp(sred *cabr,unsigned char *tuip,unsigned char *tumac);
void estructuraarp(sred *cabr,unsigned char *tuip,unsigned char *tumac);
///Inicio//
int main()
{
        int paquete=socket(AF_PACKET,SOCK_RAW,htons(ETH_P_ALL)),ttl=64,pac=4,ciclo;
		char *mensaje=(char *)malloc(100*sizeof(char));strcpy(mensaje,"");
		char *inter=(char *)malloc(32*sizeof(char));
		char *destino=(char *)malloc(32*sizeof(char));
        if(paquete==-1)
        {
        	perror("error al abrir el socket");
        	return 1;
        }else{
        		printf("Ingrese la direccion:");
        		scanf("%s",destino);
        		printf("Interfaz:");
        		scanf("%s",inter);
                parametros(destino,&ttl,&pac,tuip,mensaje);
                cabr=datosred(inter,paquete);
                putdatos(cabr);
				int tiempopac[pac];
                printf("\n\nSe hace ping a");putip(tuip);
                printf("\n\nMensaje:%s\n",mensaje);
                if(compruebared(cabr,tuip))
                {
                        printf("\n\nDestino dentro de la red\n");
                        estructuraarp(cabr,tuip,tumac);
                        for(ciclo=0;ciclo<pac;ciclo++)
                        {
                                estructuraping(cabr,tumac,tuip,ciclo+1,ttl,1,tiempopac,mensaje);
                        }
                }
                else
                {
                        puts("\nDestino fuera de la red");
                        for(ciclo=0;ciclo<pac;ciclo++)
                        {
                                estructuraping(cabr,tumac,tuip,ciclo+1,ttl,0,tiempopac,mensaje);
                        }
                }
                estadisticas(tiempopac,tuip,pac);
        }
        return 0;
}
void parametros(char *dest,int *ttl,int *pac,unsigned char *tuip,char *mensaje)
{
    //printf("LLegue\n");
    //printf("PR1=%s\n",dest);
    int flag=0;
    if(dest[0]=='w'&& dest[1]=='w' && dest[2]=='w')
    {
        char *hostip;
        hostip=hostname_to_ip(dest);
        printf("Destino:%s->IP:%s\n", dest,hostip);
        sscanf(hostip," %d.%d.%d.%d",(int*)&tuip[0],(int*)&tuip[1],(int*)&tuip[2],(int*)&tuip[3]);
        flag=1;
    }else{
        printf("Destino:%s\n", dest);
        sscanf(dest,"%d.%d.%d.%d",(int*)&tuip[0],(int*)&tuip[1],(int*)&tuip[2],(int*)&tuip[3]);
        flag=1;
    }
    if (flag==0)
    {
        printf("Destino no válido\n");
    }
}
char* hostname_to_ip(char * hostname)
{
    struct hostent *he;
    struct in_addr **addr_list;
    int i;
    char* ip;
    ip=(char*)malloc(15*sizeof(char));
    if ( (he = gethostbyname( hostname ) ) == NULL)
    {
        // get the host info
        herror("gethostbyname");
    }

    addr_list = (struct in_addr **) he->h_addr_list;

    for(i = 0; addr_list[i] != NULL; i++)
    {
        //Return the first one;
        strcpy(ip , inet_ntoa(*addr_list[i]) );
    }

    return ip;
}
int ipValida(char *ip)
{
    struct sockaddr_in adr_inet;
    int flag=1;
    if (!inet_pton(AF_INET,ip,&adr_inet.sin_addr))//devuelve 1 en caso de éxito (dirección de red se convirtió con éxito). Se devuelve 0 si src no contiene una cadena de caracteres que representa una dirección de red válida
    {
        printf("\nIngrese una IP válida");
        flag=0;
    }
    return flag;

}
void putip(unsigned char *ip)
{
	for(i=0;i<4;i++)
	{
		printf("%d",ip[i]);
		if(i==3)
			continue;
		printf(".");
	}
}
void putmac(unsigned char *mac)
{
	for(i=0;i<6;i++)
        {
                printf("%.2x",mac[i]);
                if(i==5)
                        continue;
                printf(":");
        }
}
void putdatos(sred *cabr)
{
	printf("\nDatos interfaz de red");
	printf("\nIp:");putip(cabr->ip);
	printf("\nMAC:");putmac(cabr->mac);
	printf("\nMascara:");putip(cabr->mascara);
	printf("\nIp puerta enlace:");putip(cabr->ipenlace);
	printf("\nMac puerta enlace:");putmac(cabr->macenlace);
}
sred * datosred(char *interfaz,int paq)
{
        struct ifreq infointerfaz;
	sred *nvor;
        strcpy(infointerfaz.ifr_name,interfaz);
	nvor=(sred *)malloc(sizeof(sred));
	nvor->paquete=paq;
        if(ioctl(paq,SIOCGIFINDEX,&infointerfaz)==-1)
                {perror("\nerror al obtener indice");exit(-1);}
        nvor->indice=infointerfaz.ifr_ifindex;
        if(ioctl(paq,SIOCGIFHWADDR,&infointerfaz)==-1)
                {perror("\nerro al obtener la mac");}
        else
                {memcpy(nvor->mac+0,infointerfaz.ifr_hwaddr.sa_data,6);}
        if(ioctl(paq,SIOCGIFADDR,&infointerfaz)==-1)
                {perror("\nerro al obener la ip");}
        else
                {memcpy(nvor->ip+0,infointerfaz.ifr_addr.sa_data+2,4);}
        if(ioctl(paq,SIOCGIFNETMASK,&infointerfaz)==-1)
                {perror("\nerror al obtener la mascara de subrred");}
        else
                {memcpy(nvor->mascara,infointerfaz.ifr_netmask.sa_data+2,4);}
        system("arp -an > MACdoor.txt");
        if((puerta=fopen("MACdoor.txt","r"))!=NULL)
        {
                fscanf(puerta,"? (%d.%d.%d.%d) at %x:%x:%x:%x:%x:%x",(int*)&nvor->ipenlace[0],(int*)&nvor->ipenlace[1],(int*)&nvor->ipenlace[2],(int*)&nvor->ipenlace[3],(unsigned int *)&nvor->macenlace[0],(unsigned int *)&nvor->macenlace[1],(unsigned int *)&nvor->macenlace[2],(unsigned int *)&nvor->macenlace[3],(unsigned int *)&nvor->macenlace[4],(unsigned int *)&nvor->macenlace[5]);
        }
        else
                {printf("\nError al abrir el archivo");exit(-1);}
        fclose(puerta);
        return nvor;
}
int resivearp(sred *cabr,unsigned char *tuip,unsigned char *tumac)
{
        struct timeval start, end;
        double time;
	gettimeofday(&start, NULL);
        while(time<2000)
        {
                int tam;
                tam=recvfrom(cabr->paquete,tramaarprec,48,0,NULL,0);
                if(tam==-1)
                        {perror("\nError al recibir trama");}
                else
                {
                        if(!memcmp(tramaarprec+20,codrespuesta,2)&&!memcmp(tramaarprec+0,cabr->mac,6)&&!memcmp(tramaarprec+28,tuip,4))
                        {
                                memcpy(tumac+0,tramaarprec+6,6);
				printf("\npidiendo mac ala ip ");putip(tuip);printf(" ...");
                                printf("su mac es: ");putmac(tumac);printf("\n");
                                return 0;
                        }
                }
                gettimeofday(&end, NULL);
                time =(end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000.0;
        }
	return 1;
}
void enviaarp(sred *cabr,unsigned char *tuip,unsigned char *tumac)
{
        int tam;
        struct sockaddr_ll nic;
        memset(&nic, 0x00, sizeof(nic));
        nic.sll_family = AF_PACKET;
        nic.sll_protocol = htons(ETH_P_ALL);
        nic.sll_ifindex = cabr->indice;
        tam = sendto(cabr->paquete, tramaarpenv,48, 0, (struct sockaddr *)&nic, sizeof(nic));
        if(tam == -1)
                perror("\nError al enviar\n\n");
        else
        {
                if(resivearp(cabr,tuip,tumac))
			{printf("\nrespuesta fallida\n"); exit(-1);}
        }
}
void estructuraarp(sred *cabr,unsigned char *tuip,unsigned char *tumac)
{
    memset(tumac,0xff,6);
    memcpy(tramaarpenv+0,tumac,6);
    memcpy(tramaarpenv+6,cabr->mac,6);
    memcpy(tramaarpenv+12,etertype,2);
	tramaarpenv[14]=0x00;//harware
	tramaarpenv[15]=0x01;//harware
	tramaarpenv[16]=0x08;//protocolo
	tramaarpenv[17]=0x00;//protocolo
    tramaarpenv[18]=0x06;//ldh
    tramaarpenv[19]=0x04;//ldp
    memcpy(tramaarpenv+20,codenvio,2);
    memcpy(tramaarpenv+22,cabr->mac,6);
    memcpy(tramaarpenv+28,cabr->ip,4);
    memcpy(tramaarpenv+32,tumac,6);
    memcpy(tramaarpenv+38,tuip,4);
	enviaarp(cabr,tuip,tumac);
}
void estadisticas(int *tiempopac,unsigned char *tuip,int pac)
{
	int min,max,med=0;
	min=tiempopac[0];
	max=tiempopac[0];
	for(i=0;i<ctapac[1];i++)
	{
		//printf("%d  ",tiempopac[i]);
		if(tiempopac[i]<min)
			min=tiempopac[i];
		if(tiempopac[i]>max)
			max=tiempopac[i];
		med=med+tiempopac[i];
	}
	med=med/ctapac[1];
	printf("\n\nEstadísticas:");putip(tuip);puts(":\n");
	printf("Paquetes enviados=%d, Recividos=%d, Perdidos=%d\n",ctapac[0],ctapac[1],ctapac[2]);
	printf("(%d %% ",(ctapac[2]*100)/pac);printf("perdidos),\n");
	printf("tiempos aproximados de ida y vuelta en milisegundos:\n");
	printf("minimo=%dms, maximo=%dms, media=%dms\n\n",min,max,med);
}
int compruebared(sred *cabr,unsigned char *tuip)
{
        unsigned char miipmas[4];
        unsigned char tuipmas[4];
        for(i=0;i<4;i++)
        {
                miipmas[i]=cabr->ip[i]&cabr->mascara[i];
                tuipmas[i]=tuip[i]&cabr->mascara[i];
        }
        if(!memcmp(miipmas,tuipmas,4))
                return 1;
      	return 0;
}
void checksum(int limi, int lims, unsigned char *check0, unsigned char *check1, unsigned char *mtrama)
{
	unsigned int suma=0;
	unsigned short aux;
	for(i=limi;i<lims;i+=2)
	{
		aux=mtrama[i];
		aux=(aux<<8)+mtrama[i+1];
		suma=suma+aux;
	}
	suma=(suma&0x0000FFFF)+(suma>>16);
	*check0=(unsigned char)~(suma);
        *check1=(unsigned char)~(suma>>8);
}
int reciveping(sred *cabr,unsigned char *tuip,int *tiempopac,int sec,int tamtra)
{
        int tamano;
        struct timeval start, end;
        long int ptime=0;
		gettimeofday(&start, NULL);
        while( ptime < 100)//ciclo que recibira tramas
        {
                tamano = recvfrom(cabr->paquete,tramapingrec,74,0,NULL,0);
                if(tamano == -1)
                        printf("Error al recibir ping");
                else
                {
                        if(!memcmp(tramapingrec,cabr->mac,6)&&!memcmp(tramapingrec+12,ethertypeping,2)&&!memcmp(tramapingrec+26,tuip,4)&&!memcmp(tramapingrec+30,cabr->ip,4))
                        {
                               	gettimeofday(&end, NULL);
                                ptime =(end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000;
                                ctapac[1]++;
				tiempopac[intime]=ptime;intime++;
                                printf("\nRespuesta desde: ");putip(tuip);
                       printf(": bytes=74 tiempo=%ldms ttl=%d",ptime,tramapingenv[22]);
				return(1);
                        }
                }
                gettimeofday(&end, NULL);
                ptime =(end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000;
        }
        printf("\nRespuesta desde ");putip(tuip);
        printf(": fallida time=%ldms ttl=%d",ptime,tramapingenv[22]);
	ctapac[2]++;
	return(0);
}
void enviaping(sred *cabr,unsigned char *tuip,int *tiempopac,int sec,int tamtra)
{
        struct sockaddr_ll nic;
        int tamano;
        memset(&nic, 0, sizeof(struct sockaddr_ll));
        nic.sll_family= AF_PACKET;
        nic.sll_protocol = htons(ETH_P_ALL);
        nic.sll_ifindex=cabr->indice;
        tamano =sendto(cabr->paquete,tramapingenv,tamtra,0,(struct sockaddr *)&nic, sizeof(struct sockaddr_ll));
        if(tamano == -1)
                perror("\nError al enviar ping");
	else
		{ctapac[0]++;reciveping(cabr,tuip,tiempopac,sec,tamtra);}

}
void estructuraping(sred*cabr,unsigned char *tumac,unsigned char *tuip,int secuencia,int ttl,int red,int *tiempopac,char *mensaje)
{

	memset(tramapingenv+0,0x00,74);
	if(red)
		memcpy(tramapingenv+0,tumac,6);
	else
		memcpy(tramapingenv+0,cabr->macenlace,6);
        memcpy(tramapingenv + 6,cabr->mac,6);
	tramapingenv[12]=0x08;//ethertipe ping
	tramapingenv[13]=0x00;//ethertipe ping
    ///sigue todo lo del encabezado ip
	tramapingenv[14]=0x45;//version ip
	tramapingenv[15]=0x00;//tiposervicio
	tramapingenv[16]=0x00;//longitud ip
	tramapingenv[17]=28+strlen(mensaje);//0x3c;//longitud ip
	tramapingenv[18]=rand();
	tramapingenv[19]=0x00;
	tramapingenv[20]=0x00;
	tramapingenv[21]=0x00;
    memcpy(tramapingenv +22,&ttl,1);
	tramapingenv[23]=0x01;//protocolo ping
	tramapingenv[24]=0x00;//checsum
	tramapingenv[25]=0x00;//checsum
	memcpy(tramapingenv +26,cabr->ip,4);
    memcpy(tramapingenv +30,tuip,4);
	checksum(14,33,&tramapingenv[25],&tramapingenv[24],tramapingenv);
    //ahora se llena el encabezado udp
	tramapingenv[34]=0x08;
	tramapingenv[35]=0x00;
	tramapingenv[36]=0x00;
	tramapingenv[37]=0x00;
	tramapingenv[38]=0x00;
	tramapingenv[39]=0x01;
	tramapingenv[40]=0x00;
    memcpy(tramapingenv +41,&secuencia,1);
	memcpy(tramapingenv +42,mensaje,strlen(mensaje));
	checksum(34,74,&tramapingenv[37],&tramapingenv[36],tramapingenv);
//	checksum(34,(41+strlen(mensaje)),&tramapingenv[37],&tramapingenv[36],tramapingenv);
	enviaping(cabr,tuip,tiempopac,secuencia,(42+strlen(mensaje)));
}
