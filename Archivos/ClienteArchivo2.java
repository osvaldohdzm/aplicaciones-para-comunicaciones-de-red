import javax.swing.JFileChooser;
import java.net.*;
import java.io.*;
public class ClienteArchivo2 {


		

    public static void main(String[] args) {
       try{  //SE DEFINE EL FLUJO DE ENTRADA
           BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
           System.out.printf("Escriba la dirección del servidor: ");
           String host = br.readLine();
           System.out.printf("\n\nEscriba el puerto: ");
           int pto = Integer.parseInt(br.readLine());
           //SE DEFINE EL SOCKET
           Socket cl = new Socket(host,pto);
           JFileChooser jf = new JFileChooser();//SIRVE PARA ELEGIR EL ARCCHIVO A ENVIAR
	   jf.setMultiSelectionEnabled(true);
           int r = jf.showOpenDialog(null);
           //SE OBTIENEN SUS DATOS PRINCIPALES
           if(r == JFileChooser.APPROVE_OPTION){
               File[] f = jf.getSelectedFiles();//Manejador
               String archivo;
	       String nombre;
	       long tam;

archivo = f[0].getAbsolutePath();


  DataOutputStream dos = new DataOutputStream(cl.getOutputStream());
DataInputStream dis = new DataInputStream(new FileInputStream(archivo));



for (int i = 0; i < f.length ; i++)
{

		archivo = f[i].getAbsolutePath();//Dirección
                nombre = f[i].getName(); //Nombre
               tam = f[i].length();//Tamaño

               //DEFINICION DE DOS FLUJOS PARA LEER Y ENVIAR DATOS POR EL SOCKET
               
            dis = new DataInputStream(new FileInputStream(archivo));
               //SE ENVIAN LOS DATOS GENERALES
               dos.writeUTF(nombre);
               dos.flush();
               dos.writeLong(tam);
	       dos.flush();
   


               //LEO DATOS EN EL ARCHIVO 1024 Y LO ENVIAMOS POR SOCKET
               byte[] buf = new byte[1024];
               long enviados = 0;
               int porcentaje, n;
               while(enviados < tam){
                   n = dis.read(buf);
                   dos.write(buf,0,n);
                   dos.flush();
                   enviados = enviados + n;
                   porcentaje = (int)(enviados*100/tam);
                   System.out.print("Enviado: "+porcentaje+"%\r");
               }
             
               

}

//CERRAMOS LO FLUJOS, SOCKET Y LA CLASE
               System.out.print("\n\nArchivos enviado");
		dos.close();
               dis.close();
               cl.close();

           } 
       }catch(Exception e){
           e.printStackTrace();
       }
    }
    
}
