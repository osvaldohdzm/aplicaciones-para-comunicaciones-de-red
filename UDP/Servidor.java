import java.net.*;
import java.io.*;

public class Servidor{
  public static void main(String[] args) {
    try{
      BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
      int pto=9000;
      String dst = "127.0.0.1";

      System.out.println("Escribe un mensaje: ");
      String msj = br.readLine();
      byte[] b= msj.getBytes();
      DatagramSocket cl = new DatagramSocket();
      System.out.println("Enviando mensaje...");
      DatagramPacket p = new DatagramPacket(b, b.length, InetAddress.getByName(dst), pto);
      cl.send(p);
      System.out.println("Mensaje enviado");
      cl.close();
    }catch (Exception e) {
      e.printStackTrace();
    }
  }
}