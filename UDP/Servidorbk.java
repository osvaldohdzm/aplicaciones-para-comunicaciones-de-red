import java.net.*;
import java.io.*;
public class Servidor {
public static void main(String[] args){
try{
DatagramSocket s = new DatagramSocket(2000);
DatagramPacket p = new DatagramPacket(new byte[65535],65535); // Tamaño máximo UDP
System.out.println("Servidor iniciado, esperando cliente");
for(;;){
	p = new DatagramPacket(new byte[65535],65535); 
	s.receive(p);
	System.out.println("Datagrama recibido desde"+p.getAddress()+":"+p.getPort());
	String msj = new String(p.getData(),0,p.getLength());
	System.out.println("Con el mensaje:"+ msj);
	System.gc();
}//for
//s.close()
}catch(Exception e){
e.printStackTrace();
}//catch
}//main
}