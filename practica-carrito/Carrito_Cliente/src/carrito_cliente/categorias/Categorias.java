package categorias;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author jdiaz
 */
public class Categorias implements Serializable{
    ArrayList<Categoria> categorias;

    public Categorias(ArrayList<Categoria> categorias) {
        this.categorias = categorias;
    }

    public ArrayList<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(ArrayList<Categoria> categorias) {
        this.categorias = categorias;
    }
    
}
