package carrito_cliente.sistema.esperadores.imagenes;

import archivos.InfoArchivo;
import carrito_cliente.Cliente;
import carrito_cliente.sistema.peticiones.GestionadorPeticiones;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import peticiones.Peticion;

/**
 *
 * @author jdiaz
 */
public class EsperadorImagen implements Runnable{
    private final Cliente cliente;
    private volatile boolean banderaImagen;
    private String nombreImagen;
    private Object objeto;
    private int pesoImagen;
    public EsperadorImagen(Cliente cliente, String nombreImagen, int pesoImagen) {
        this.nombreImagen=nombreImagen;
        this.cliente = cliente;
        this.pesoImagen=pesoImagen;
        banderaImagen=false;
    }
    
    @Override
    public void run() {
        while(true){
            //vamos a recibir una imagen
            System.out.println("While recibir imagen");
            if(banderaImagen==true){
                System.out.println("saliendo del while");
                break;
            }
            recibirImagen(nombreImagen,pesoImagen);
        }
        GestionadorPeticiones gpeticiones=new GestionadorPeticiones(cliente);
        gpeticiones.hacerPeticion(new Peticion("/EnviaOtraImagenCatalogo"));
        System.out.println("peticion de otra enviada");
        cliente.esperarRespuestas();
        cliente.terminarEsperarImagen();
    }
    public void recibirImagen(String nombreImagen, int peso){
        System.out.println("Metodo recibir imagen");
        try {
            InputStream input = cliente.getConexion().getInputStream(); 
            FileOutputStream wr = new FileOutputStream(new File("./img/catalog/"+nombreImagen));
            byte[] buffer = new byte[8192];
            int bytesReceived = 0;
            int auxBytes=0;
            //total < imageSize && (count = in.read(buffer, 0, (int)Math.MIN(buffer.length, imageSize-total))) > 0
            while(auxBytes < peso && (bytesReceived = input.read(buffer,0,(int)Math.min(buffer.length,peso-auxBytes)))>0){
                auxBytes=auxBytes+bytesReceived;
                //System.out.println("bytesReceived: " + bytesReceived);
                wr.write(buffer,0,bytesReceived);
                //System.out.println("aux bytes "+auxBytes+ " y peso "+peso );
                if(auxBytes==peso){
                    System.out.println("total imagen bytes recibidos");
                    break;
                }
            } 
            System.out.println("Imagen Recibida");
            banderaImagen=true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EsperadorImagen.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EsperadorImagen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
