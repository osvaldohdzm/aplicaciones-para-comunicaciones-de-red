/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrito_cliente.sistema.esperadores.pdf;

import carrito_cliente.Cliente;
import carrito_cliente.sistema.esperadores.imagenes.EsperadorImagen;
import carrito_cliente.sistema.peticiones.GestionadorPeticiones;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import peticiones.Peticion;

/**
 *
 * @author jdiaz
 */
public class EsperadorPDF implements Runnable{
    private final Cliente cliente;
    private volatile boolean bandera;
    private String nombrepdf;
    private Object objeto;
    private int pesopdf;

    public EsperadorPDF(Cliente cliente,String nombre,int peso) {
        this.cliente = cliente;
        this.nombrepdf=nombre;
        this.pesopdf=peso;
    }

    @Override
    public void run() {
       while(true){
            //vamos a recibir una imagen
            System.out.println("While recibir pdf");
            if(bandera==true){
                System.out.println("saliendo del while");
                break;
            }
            recibirPDF(nombrepdf,pesopdf);
        }
        cliente.esperarRespuestas();
        cliente.terminarEsperarPDF();
    }
    public void recibirPDF(String nombrepdf,int peso){
        System.out.println("Metodo recibir pdf");
        try {
            InputStream input = cliente.getConexion().getInputStream(); 
            FileOutputStream wr = new FileOutputStream(new File("./tickets/"+nombrepdf));
            byte[] buffer = new byte[8192];
            int bytesReceived = 0;
            int auxBytes=0;
            //total < imageSize && (count = in.read(buffer, 0, (int)Math.MIN(buffer.length, imageSize-total))) > 0
            while(auxBytes < peso && (bytesReceived = input.read(buffer,0,(int)Math.min(buffer.length,peso-auxBytes)))>0){
                auxBytes=auxBytes+bytesReceived;
                //System.out.println("bytesReceived: " + bytesReceived);
                wr.write(buffer,0,bytesReceived);
                //System.out.println("aux bytes "+auxBytes+ " y peso "+peso );
                if(auxBytes==peso){
                    System.out.println("total pdf bytes recibidos");
                    break;
                }
            } 
            System.out.println("PDF recibido");
            bandera=true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EsperadorPDF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EsperadorPDF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public boolean isBandera() {
        return bandera;
    }

    public void setBandera(boolean bandera) {
        this.bandera = bandera;
    }
    
}