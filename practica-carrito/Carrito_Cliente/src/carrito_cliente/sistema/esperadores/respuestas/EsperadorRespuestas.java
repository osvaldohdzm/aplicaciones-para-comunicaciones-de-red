package carrito_cliente.sistema.esperadores.respuestas;

import archivos.InfoArchivo;
import carrito_cliente.Cliente;
import carrito_cliente.sistema.peticiones.GestionadorPeticiones;
import categorias.Categorias;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import peticiones.Peticion;
import productos.Productos;
import respuestas.Respuesta;

/**
 *
 * @author jdiaz
 */
public class EsperadorRespuestas implements Runnable{
    private final Cliente cliente;
    private Object objeto;
    
    public EsperadorRespuestas(Cliente cliente) {
        this.cliente = cliente;
    }
    
    @Override
    public void run() {
       while(true){
           try {
               objeto=(Object)cliente.getIn().readObject();
               System.out.println("soy Respuesta"+objeto);
               if(objeto instanceof Respuesta){
                   System.out.println("Era Respuesta");
                   Respuesta respuesta=(Respuesta)objeto;
                   analizarRespuesta(respuesta);
               }
               if(objeto instanceof Productos){
                   System.out.println("Era Productos");
                   Productos productos=(Productos)objeto;
                   cliente.getGproductos().setProductos(productos.getProductos());
                   System.out.println("Productos enviados a gestionador");
                   //Ahora pediremos las categorias
                   cliente.pedirCategorias();
                   //cliente.getControl().setProductos();
                   cliente.getGproductos().actualizarRutaImagenes();
               }
               if(objeto instanceof Categorias){
                   System.out.println("Era Categorias");
                   Categorias categorias=(Categorias)objeto;
                   cliente.getGcategorias().setCategorias(categorias.getCategorias());
                   System.out.println("Categorias enviados a gestionador");
                   System.out.println("primera categoria "+categorias.getCategorias().get(0));
                   //Ahora pediremos las categorias
                   cliente.getControl().setCategorias();
               }
               if(objeto instanceof InfoArchivo){
                    System.out.println("Era info Archivo");
                    InfoArchivo infoArchivo=(InfoArchivo)objeto;
                    //EL server nos ha enviado info de un archivo ¿Que archivo es?
                    String tipoArchivo=infoArchivo.getTipo();
                    if(tipoArchivo.equals("pdf")){
                        String nombre=infoArchivo.getNombre();
                        int peso=infoArchivo.getPeso();
                        System.out.println("El nombre del pdf es " + nombre);
                        if(nombre.equals("/TerminePDF")){
                            //ya no hay mas imagenes que recibir
                            System.out.println("El server termino de enviar pdf");
                            System.out.println("---------------------Significa que ya recibimos pdf y todo salio bien------------------------------------");
                            cliente.getControl().mostrarAviso("Ticket", "Tu Ticket se ha generado!");
                            cliente.getControl().limpiarCarrito();
                            cliente.getControl().limpiarCatalogo();
                            //cliente.getControl().pedirImagenesCatalogoConsecuente();
                            cliente.getControl().preguntarTicket();
                            
                        }else{
                            //Significa que el server nos va a enviar una imagen, vamos a decirle que la envie
                            cliente.esperarPDF(nombre,peso);
                            GestionadorPeticiones gpeticiones=new GestionadorPeticiones(cliente);
                            gpeticiones.hacerPeticion(new Peticion("/EnviaTicket"));
                            break;
                        }
                        cliente.esperarPDF(nombre, peso);
                    }
                    if(tipoArchivo.equals("imagen")){
                        String nombreImagen=infoArchivo.getNombre();
                        int pesoImagen=infoArchivo.getPeso();
                        System.out.println("El nombre de la imagen es " + nombreImagen);
                        if(nombreImagen.equals("/TermineImagenes")){
                            //ya no hay mas imagenes que recibir
                            System.out.println("El server termino de enviar imagenes");
                            cliente.getControl().mostrarCatalogo();
                            //Hay que pedir ahora los productos
                            cliente.pedirProductos();
                        }else{
                            //Significa que el server nos va a enviar una imagen, vamos a decirle que la envie
                            cliente.esperarImagen(nombreImagen,pesoImagen);
                            GestionadorPeticiones gpeticiones=new GestionadorPeticiones(cliente);
                            gpeticiones.hacerPeticion(new Peticion("/EnviaSiguienteImagen"));
                            break;
                        }
                    }
                }
           } catch (IOException | ClassNotFoundException ex) {
               Logger.getLogger(EsperadorRespuestas.class.getName()).log(Level.SEVERE, null, ex);
               cliente.getControl().mostrarError("Error", "El server se ha desconectado, Respuestas");
               break;
           }
       }
       System.out.println("Salimos del while");
       //Si salimos significa que hay que tirar este hilo
       cliente.terminarEsperaRespuestas();
    }
    public void analizarRespuesta(Respuesta respuesta){
        System.out.println(respuesta.getRespuesta());
        switch(respuesta.getRespuesta()){
            case "/ImagenesCatalogo":
                {
                    System.out.println("El server esta listo para enviar una imagen");
                    //por ahora ya no vamos a recibir respuestas del server ahora seran archivos
                    GestionadorPeticiones gpeticiones=new GestionadorPeticiones(cliente);
                    //Esto hace que el server nos envie la primera imagen
                    gpeticiones.hacerPeticion(new Peticion("/EnviaOtraImagenCatalogo"));
                }
            break;
            case "/ProductosCatalogo":
                {
                    System.out.println("El server esta listo para los productos");
                    GestionadorPeticiones gpeticiones=new GestionadorPeticiones(cliente);
                    gpeticiones.hacerPeticion(new Peticion("/EnviaProductos"));
                }
            break;
            case "/CategoriasCatalogo":
                {
                    System.out.println("El server esta listo para las categorias");
                    GestionadorPeticiones gpeticiones=new GestionadorPeticiones(cliente);
                    gpeticiones.hacerPeticion(new Peticion("/EnviaCategorias"));
                }
            break;
            case "/CompraGuardada":
                {
                    System.out.println("---------------------Significa que la compra se realizo correctamente------------------------------------");
                    GestionadorPeticiones gpeticiones=new GestionadorPeticiones(cliente);
                    gpeticiones.hacerPeticion(new Peticion("/EnviaInfoTicket"));
                }
            break;
        }
    }
}
