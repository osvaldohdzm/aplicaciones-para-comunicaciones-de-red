package carrito_cliente.sistema.carrito;

import carrito.Carrito;
import carrito_cliente.Cliente;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import productos.Producto;

/**
 *
 * @author jdiaz
 */
public class GestionadorCarrito {
    private final Cliente cliente;
    private ArrayList<Producto> articulosComprados;
    int subtotal;
    double total;
    int noArticulos;

    public GestionadorCarrito(Cliente cliente) {
        this.cliente = cliente;
        articulosComprados=new ArrayList<>();
        subtotal=0;
        total=0;
        noArticulos=0;
    }
    public void anadirProducto(Producto producto){
        int existencia=producto.getExistencia();
        if (existencia > 0){
            producto.setExistencia(existencia-1);
            articulosComprados.add(producto);
            subtotal=subtotal+Integer.parseInt(producto.getPrecio());
            total=subtotal+subtotal*(0.16);
            noArticulos++;
        }else{
            cliente.getControl().mostrarAviso("Ya no hay existencía", "No contamos con más de ese producto");
        }
    }
    public ArrayList<Producto> getArticulosComprados() {
        return articulosComprados;
    }
    public boolean quitarProductoCarrito(int id){
        //en el array que gestiona esta clase, vamos a recorrerlo
        //para buscar que productos tienen el id que estamos buscando y una ves encontrado eliminarlo
        Producto producto=cliente.getGproductos().getProductoPorID(id);
        int existencia=producto.getExistencia();
        producto.setExistencia(existencia+1);
        if(articulosComprados.removeIf(p->p.getIdProducto().equals(Integer.toString(id)))){
            subtotal=subtotal-Integer.parseInt(producto.getPrecio());
            total=subtotal+subtotal*(0.16);
            noArticulos--;
            cliente.getControl().mostrarAviso("Articulo removido", "El articulo ha sido removido");
            return true;
        }else{
            cliente.getControl().mostrarAviso("Error", "El articulo no se encuentra en el carrito");
            return false;
        }
    }
    public void enviarCompra(){
        try {
            Carrito carrito=new Carrito(this.noArticulos,this.articulosComprados,this.subtotal,total);
            cliente.getOut().writeObject(carrito);
            cliente.getOut().flush();
            System.out.println("carrito enviado");
            resetTotales();
        } catch (IOException ex) {
            Logger.getLogger(GestionadorCarrito.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public int getSubtotal() {
        return subtotal;
    }

    public double getTotal() {
        return total;
    }

    public int getNoArticulos() {
        return noArticulos;
    }
    public void resetTotales(){
        this.subtotal = 0;
        this.total = 0;
        this.noArticulos = 0;
        articulosComprados.clear();
    }
}
