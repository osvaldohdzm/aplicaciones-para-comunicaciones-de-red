package carrito_cliente.sistema.categorias;

import carrito_cliente.Cliente;
import categorias.Categoria;
import java.util.ArrayList;

/**
 *
 * @author jdiaz
 */
public class GestionadorCategorias {
    private final Cliente cliente;
    private ArrayList<Categoria> categorias;

    public GestionadorCategorias(Cliente cliente) {
        this.cliente = cliente;
    }

    public ArrayList<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(ArrayList<Categoria> categorias) {
        this.categorias = categorias;
    }

    
    public void destroy(){
        categorias.clear();
        categorias=null;
    }
}
