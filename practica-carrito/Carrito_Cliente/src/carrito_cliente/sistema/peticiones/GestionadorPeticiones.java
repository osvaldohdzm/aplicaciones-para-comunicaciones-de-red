package carrito_cliente.sistema.peticiones;

import carrito_cliente.Cliente;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import peticiones.Peticion;

/**
 *
 * @author jdiaz
 */
public class GestionadorPeticiones {
    private Cliente cliente;

    public GestionadorPeticiones(Cliente cliente) {
        this.cliente = cliente;
    }
    public void hacerPeticion(Peticion peticion){
        try {
            cliente.getOut().writeObject(peticion);
            cliente.getOut().flush();
            System.out.println("Peticion enviada");
        } catch (IOException ex) {
            Logger.getLogger(GestionadorPeticiones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
