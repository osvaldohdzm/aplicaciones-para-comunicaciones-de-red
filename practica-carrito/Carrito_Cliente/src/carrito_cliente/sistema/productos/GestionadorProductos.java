package carrito_cliente.sistema.productos;

import carrito_cliente.Cliente;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Stream;
import productos.Producto;
import productos.Productos;

/**
 *
 * @author jdiaz
 */
public class GestionadorProductos {
    private Cliente cliente;
    private ArrayList<Producto> productos;

    public GestionadorProductos(Cliente cliente) {
        this.cliente = cliente;
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }
    public void actualizarRutaImagenes(){
        System.out.println("actualizar ruta");
        Producto producto;
        for(int i=0;i<productos.size();i++){
            producto=productos.get(i);
            //obtener nombre imagen
            String auxToken="";
            int auxTokens=0;
            StringTokenizer separador = new StringTokenizer(producto.getImagen(),"\\",true);
            List<String> auxSeparador=new ArrayList<>();
            while (separador.hasMoreTokens()) {
                auxToken=separador.nextToken();
                //System.out.println(auxToken);
                auxSeparador.add(auxToken);
                auxTokens++;
            }
            String nombreImagen=auxSeparador.get(auxTokens-1);
            //System.out.println(nombreImagen);
            String rutaImagen="./img/catalog/"+nombreImagen;
            producto.setImagen(rutaImagen);
            productos.set(i, producto);
        }
        /*for(int i=0;i<productos.size();i++){
            producto=productos.get(i);
            //System.out.println(producto.getImagen());
        }*/
    }
    public void setProductos(ArrayList<Producto> productos) {
        this.productos = productos;
    }
    public void destroy(){
        productos.clear();
        productos=null;
    }
    public Producto getProductoPorID(int id){
        Object[] productosaux= productos.stream().filter(p -> p.getIdProducto().equals(Integer.toString(id))).toArray();
        Producto p=(Producto)productosaux[0];
        return p;
    }
    public Object[] getProductosPorNombre(String nombre){
        Object[] productosstream=productos.stream().filter(p -> p.getNombre().startsWith(nombre)).toArray();
        return productosstream;   
    }
    public Object[] getProductosPorCategoria(String categoria){
        Object[] productosstream=productos.stream().filter(p -> p.getCategoria().equals(categoria)).toArray();
        return productosstream;   
    }
    /*public Stream<Producto> getProductoPorNombre(String nombre){
        Stream<Producto> productosstream=productos.stream().filter(p -> p.getNombre().equals(nombre));
        return productosstream;   
    }*/
}
