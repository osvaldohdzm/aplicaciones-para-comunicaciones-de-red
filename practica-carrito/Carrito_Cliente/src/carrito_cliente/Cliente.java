package carrito_cliente;

import carrito_cliente.interfaz.control.ControlCliente;
import carrito_cliente.sistema.carrito.GestionadorCarrito;
import carrito_cliente.sistema.categorias.GestionadorCategorias;
import carrito_cliente.sistema.esperadores.imagenes.EsperadorImagen;
import carrito_cliente.sistema.esperadores.pdf.EsperadorPDF;
import carrito_cliente.sistema.esperadores.respuestas.EsperadorRespuestas;
import carrito_cliente.sistema.peticiones.GestionadorPeticiones;
import carrito_cliente.sistema.productos.GestionadorProductos;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import peticiones.Peticion;

/**
 *
 * @author 
 */
public class Cliente {
    private final ControlCliente control;
    private int puerto;
    private String hostName;
    private Socket conexion;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private ExecutorService esperadorRespuestas;
    private ExecutorService esperadorImagen;
    private ExecutorService esperadorPDF;
    private GestionadorProductos gproductos;
    private GestionadorCategorias gcategorias;
    private GestionadorCarrito gcarrito;
    
    public Cliente(ControlCliente control) {
        this.control=control;
        puerto=0;
        hostName=null;
        conexion=null;
        gproductos=null;
        gcarrito=null;
        gcategorias=null;
    }
    public int conectarConServidor(int puerto,String host){
        System.out.println("Conectar Con servidor");
        this.puerto=puerto;
        this.hostName=host;
        try {
            conexion = new Socket(hostName, puerto);
            out= new ObjectOutputStream(conexion.getOutputStream());
            in= new ObjectInputStream(conexion.getInputStream());   
            return 1;
        } catch (IOException ex) {
            control.mostrarError("Conexión", "Ocurrio un error al conectarse con el servidor");
            return 0;
        }
    }
    public void desconectarConServidor(){
        try {
            conexion.close();
            conexion=null;
            this.terminarEsperarImagen();
            this.terminarEsperaRespuestas();
            this.gproductos.destroy();
            this.gproductos=null;
            System.gc();
            System.exit(0);
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            control.mostrarError("Conexión", "Ocurrio un error al desconectarse con el servidor");
        }
    }
    public void pedirProductos(){
        GestionadorPeticiones gpeticiones=new GestionadorPeticiones(this);
        gpeticiones.hacerPeticion(new Peticion("/ProductosCatalogo"));
    }
    public void pedirCategorias(){
        GestionadorPeticiones gpeticiones=new GestionadorPeticiones(this);
        gpeticiones.hacerPeticion(new Peticion("/CategoriasCatalogo"));
    }
    public void esperarRespuestas(){
        esperadorRespuestas = Executors.newCachedThreadPool();
        esperadorRespuestas.execute(new EsperadorRespuestas(this));
    }
    public void terminarEsperaRespuestas(){
        esperadorRespuestas.shutdownNow();
        esperadorRespuestas=null;
        System.gc();
    }
     public void esperarImagen(String nombreImagen, int pesoImagen){
        esperadorImagen = Executors.newCachedThreadPool();
        esperadorImagen.execute(new EsperadorImagen(this, nombreImagen,pesoImagen));
    }
    public void terminarEsperarImagen(){
        esperadorImagen.shutdownNow();
        esperadorImagen=null;
        System.gc();
    }
     public void esperarPDF(String nombrePDF, int pesoPDF){
        esperadorPDF = Executors.newCachedThreadPool();
        esperadorPDF.execute(new EsperadorPDF(this, nombrePDF,pesoPDF));
    }
    public void terminarEsperarPDF(){
        esperadorPDF.shutdownNow();
        esperadorPDF=null;
        GestionadorPeticiones gpeticiones=new GestionadorPeticiones(this);
        gpeticiones.hacerPeticion(new Peticion("/TicketRecibido"));
        System.gc();
    }
    public void iniciarGestionProductos(){
        gproductos=new GestionadorProductos(this);
    }
    public void iniciarGestionCategorias(){
        gcategorias=new GestionadorCategorias(this);
    }
    public void iniciarGestionCarrito(){
        gcarrito=new GestionadorCarrito(this);
    }
    public ControlCliente getControl() {
        return control;
    }

    public Socket getConexion() {
        return conexion;
    }

    public ObjectOutputStream getOut() {
        return out;
    }

    public ObjectInputStream getIn() {
        return in;
    }
    public GestionadorProductos getGproductos() {
        return gproductos;
    }

    public GestionadorCategorias getGcategorias() {
        return gcategorias;
    }

    public GestionadorCarrito getGcarrito() {
        return gcarrito;
    }
    
}
