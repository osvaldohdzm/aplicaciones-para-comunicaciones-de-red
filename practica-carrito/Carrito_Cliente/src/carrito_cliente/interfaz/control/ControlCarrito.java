/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrito_cliente.interfaz.control;

import carrito_cliente.Cliente;
import carrito_cliente.interfaz.ICliente;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import productos.Producto;

/**
 *
 * @author jdiaz
 */
public class ControlCarrito implements ListSelectionListener {
    private ICliente frame;
    private Cliente cliente;
    public ControlCarrito(ICliente frame) {
        this.frame = frame;
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        try{
            String selectedData = null;
            int[] selectedRow = frame.tableCarrito.getSelectedRows();
            int[] selectedColumns = frame.tableCarrito.getSelectedColumns();
            for (int i = 0; i < selectedRow.length; i++) {
              for (int j = 0; j < selectedColumns.length; j++) {
                selectedData = (String) frame.tableCarrito.getValueAt(selectedRow[i], selectedColumns[j]);
              }
            }
            //frame.tableCarrito.clearSelection();
            System.out.println(selectedData);
            if(e.getValueIsAdjusting() && selectedData!=null){
                if(selectedColumns[0]==0){
                    String seleccion=JOptionPane.showInputDialog(frame, "¿Deseas quitar este producto del carrito?");
                    if(seleccion.equals("si")){
                        Producto productoSeleccionado=cliente.getGproductos().getProductoPorID(Integer.parseInt(selectedData));
                        System.out.println("El producto seleccionado para quitar tiene por nombre "+productoSeleccionado.getNombre());
                        if(cliente.getGcarrito().quitarProductoCarrito(Integer.parseInt(selectedData))){
                            System.out.println("Encontrado en array de carrito  y quitado");
                            cliente.getControl().quitarProductoTablaCarrito(selectedRow[0]);
                            //actualizarTabla();
                        }
                    }else{
                        this.mostrarAviso("Aviso", "Decidete");
                    }
                }
            }
            frame.tableCarrito.clearSelection();
        }catch(ClassCastException ex){
            this.mostrarAviso("Aviso", "Seleccione la columna ID para agregar al carrito");
        }
    }
    public void actualizarTabla(){
        DefaultTableModel temp = (DefaultTableModel) frame.tableCarrito.getModel();
        int a =temp.getRowCount()-1;
            for(int i=0; i<a; i++)
                temp.removeRow(0);
    }
    public Cliente getCliente() {
        return cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    public void mostrarError(String titulo,String error){
        JOptionPane.showMessageDialog(frame,error,titulo,JOptionPane.ERROR_MESSAGE);
    }
    public void mostrarAviso(String titulo,String aviso){
        JOptionPane.showMessageDialog(frame,aviso,titulo,JOptionPane.INFORMATION_MESSAGE);
    } 
    
}
