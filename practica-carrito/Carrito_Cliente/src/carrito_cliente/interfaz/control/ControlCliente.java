package carrito_cliente.interfaz.control;

import carrito_cliente.interfaz.ICliente;
import carrito_cliente.Cliente;
import carrito_cliente.interfaz.IconCellRenderer;
import carrito_cliente.sistema.peticiones.GestionadorPeticiones;
import categorias.Categoria;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import peticiones.Peticion;
import productos.Producto;

/**
 *
 * @author jdiaz
 */
public class ControlCliente implements ActionListener,KeyListener,ListSelectionListener{
    private final ICliente frame;
    private Cliente cliente;
    
    
    public ControlCliente(ICliente frame) {
        this.frame=frame;
        cliente=null;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "login":
            {
                System.out.println("Login");
                cliente=new Cliente(this);
                int error=cliente.conectarConServidor(Integer.parseInt(frame.fieldPuerto.getText()), frame.fieldIP.getText());
                if(error==1){
                    this.mostrarAviso("Conectado", "Conexión establecida");
                    pedirImagenesCatalogo();
                    cliente.iniciarGestionProductos();
                    cliente.iniciarGestionCategorias();
                    cliente.iniciarGestionCarrito();
                }else{
                    this.mostrarAviso("Intentelo de nuevo", "Revise la IP y puertos e intentelo de nuevo");
                }
            }
            break;
            case "buttonVerTodosProducto":
            { 
                pedirImagenesCatalogoConsecuente();
                setProductos();
            }
            break;
            case "buttonLookProducto":
            { 
                pedirImagenesCatalogoConsecuente();
                Producto producto=cliente.getGproductos().getProductoPorID(Integer.parseInt(frame.fieldIDBuscarProducto.getText()));
                frame.labelLookNombre.setText(producto.getNombre());
                frame.labelLookID.setText(producto.getIdProducto());
                frame.areaLookDescripcion.append(producto.getDescripcion());
                frame.labelLookPrecio.setText(producto.getPrecio());
                frame.labelLookCategoria.setText(producto.getCategoria());
                frame.labelLookExistencia.setText(Integer.toString(producto.getExistencia()));
                File imagen = new File(producto.getImagen());
                Image bi;
                try {
                    bi = ImageIO.read(imagen);
                    frame.labelLookImage.setIcon(new ImageIcon(bi.getScaledInstance(195, 186, Image.SCALE_DEFAULT)));
                } catch (IOException ex) {
                    Logger.getLogger(ControlCliente.class.getName()).log(Level.SEVERE, null, ex);
                    this.mostrarError("Error", "No se encontro la imagen del producto");
                }
            }
            break;
            case "comboBuscarProductoCategoria":
            { 
                pedirImagenesCatalogoConsecuente();
                //Se cargan las columnas para la tabla
                    DefaultTableModel datos = new DefaultTableModel();
                    datos.addColumn("ID");
                    datos.addColumn("Nombre");
                    datos.addColumn("Categoria");
                    datos.addColumn("Precio");
                    datos.addColumn("Existencia");
                    //Consultamos todos los productos
                    Object[] productos=cliente.getGproductos().getProductosPorCategoria(frame.comboBuscarProductoCategoria.getSelectedItem().toString());
                    frame.tableProductosCatalogo.setDefaultRenderer(Object.class,new IconCellRenderer());
                    //llenar tabla---------------------------------
                    int noproductos=productos.length;

                    Producto p;
                    for(int i=0;i<noproductos;i++){
                        p=(Producto)productos[i];
                        Object [] filaTabla = new Object[5];
                        filaTabla[0]=p.getIdProducto();//ID
                        filaTabla[1]=p.getNombre();//Nombre
                        filaTabla[2]=p.getCategoria();//Categoria
                        filaTabla[3]=p.getPrecio();//Precio
                        filaTabla[4]=p.getExistencia();//Existencia
                        datos.addRow(filaTabla);
                    }
                    frame.tableProductosCatalogo.setModel(datos);
                    this.resizeColumnWidth(frame.tableProductosCatalogo);

            }
            break;
            case "buttonComprar":
                //Tenemos que hacer un carrito y enviarlo al cliente
                cliente.getGcarrito().enviarCompra();
            break;
        }
    }
    public void mostrarCatalogo(){
        CardLayout clienteContainer = (CardLayout)frame.clienteContainer.getLayout();
        clienteContainer.show(frame.clienteContainer, "panelCarrito");
    }
    public void mostrarError(String titulo,String error){
        JOptionPane.showMessageDialog(frame,error,titulo,JOptionPane.ERROR_MESSAGE);
    }
    public void mostrarAviso(String titulo,String aviso){
        JOptionPane.showMessageDialog(frame,aviso,titulo,JOptionPane.INFORMATION_MESSAGE);
    } 
    public void pedirImagenesCatalogo(){
        GestionadorPeticiones gpeticiones=new GestionadorPeticiones(cliente);
        gpeticiones.hacerPeticion(new Peticion("/ImagenesCatalogo"));
        cliente.esperarRespuestas();
    }
    public void pedirImagenesCatalogoConsecuente(){
        GestionadorPeticiones gpeticiones=new GestionadorPeticiones(cliente);
        gpeticiones.hacerPeticion(new Peticion("/ImagenesCatalogo"));
    }
    public void setCategorias(){
        ArrayList<Categoria> categorias=cliente.getGcategorias().getCategorias();
        String aux[]=new String[categorias.size()];
        for(int i=0;i<categorias.size();i++){
            aux[i]=categorias.get(i).getNombre();
        }
        DefaultComboBoxModel model = new DefaultComboBoxModel(aux);//Recibe array
        frame.comboBuscarProductoCategoria.setModel(model);
    }
    public void setProductos(){
        //Se cargan las columnas para la tabla
        DefaultTableModel datos = new DefaultTableModel();
        datos.addColumn("ID");
        datos.addColumn("Nombre");
        datos.addColumn("Categoria");
        datos.addColumn("Precio");
        datos.addColumn("Existencia");
        //Consultamos todos los productos
        ArrayList<Producto> productos=cliente.getGproductos().getProductos();
        frame.tableProductosCatalogo.setDefaultRenderer(Object.class,new IconCellRenderer());
        //llenar tabla---------------------------------
        int noproductos=productos.size();

        for(int i=0;i<noproductos;i++){
            Object [] filaTabla = new Object[5];
            filaTabla[0]=productos.get(i).getIdProducto();//ID
            filaTabla[1]=productos.get(i).getNombre();//Nombre
            filaTabla[2]=productos.get(i).getCategoria();//Categoria
            filaTabla[3]=productos.get(i).getPrecio();//Precio
            filaTabla[4]=productos.get(i).getExistencia();//Existencia
            datos.addRow(filaTabla);
        }
        frame.tableProductosCatalogo.setModel(datos);
        this.resizeColumnWidth(frame.tableProductosCatalogo);
    }     
         /*---------------------------------------------------*/
    private void resizeColumnWidth(JTable table) {
    final TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            int width = 50; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width +1 , width);
            }
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        switch(e.getComponent().getName()){
           case "fieldBuscarProductoNombre":
                {
                    pedirImagenesCatalogoConsecuente();
                    //Se cargan las columnas para la tabla
                    DefaultTableModel datos = new DefaultTableModel();
                    datos.addColumn("ID");
                    datos.addColumn("Nombre");
                    datos.addColumn("Categoria");
                    datos.addColumn("Precio");
                    datos.addColumn("Excistencia");
                    //Consultamos todos los productos
                    Object[] productos=cliente.getGproductos().getProductosPorNombre(frame.fieldBuscarProductoNombre.getText());
                    frame.tableProductosCatalogo.setDefaultRenderer(Object.class,new IconCellRenderer());
                    //llenar tabla---------------------------------
                    int noproductos=productos.length;
                    Producto p;
                    for(int i=0;i<noproductos;i++){
                        p=(Producto)productos[i];
                        Object [] filaTabla = new Object[5];
                        filaTabla[0]=p.getIdProducto();//ID
                        filaTabla[1]=p.getNombre();//Nombre
                        filaTabla[2]=p.getCategoria();//Categoria
                        filaTabla[3]=p.getPrecio();//Precio
                        filaTabla[4]=p.getExistencia();//Existencia
                        datos.addRow(filaTabla);
                    }
                    frame.tableProductosCatalogo.setModel(datos);
                    this.resizeColumnWidth(frame.tableProductosCatalogo);
                }
           break;
       }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        //añadir a carrito, se agrega por id independiente del modo de busqueda del carrito
        try{
            if(frame.controlcarrito.getCliente()==null){
                frame.controlcarrito.setCliente(cliente);
            }
            String selectedData = null;
            int selectedrow=0;
            int[] selectedRow = frame.tableProductosCatalogo.getSelectedRows();
            int[] selectedColumns = frame.tableProductosCatalogo.getSelectedColumns();
            for (int i = 0; i < selectedRow.length; i++) {
              for (int j = 0; j < selectedColumns.length; j++) {
                selectedData = (String) frame.tableProductosCatalogo.getValueAt(selectedRow[i], selectedColumns[j]).toString();
              }
            }
             if(selectedData != null && !selectedData.equals("")){
                if(e.getValueIsAdjusting()){
                    if(selectedColumns[0]==0){
                        int seleccion=JOptionPane.showConfirmDialog(frame,"Agregar producto?", "Agregar al carrito", JOptionPane.YES_NO_OPTION);
                                //JOptionPane.showInputDialog(frame, "¿Desea agregar este producto al carrito?");
                        if(seleccion == JOptionPane.YES_OPTION){
                            Producto productoSeleccionado=cliente.getGproductos().getProductoPorID(Integer.parseInt(selectedData));
                            System.out.println("El producto seleccionado tiene por nombre "+productoSeleccionado.getNombre());
                            cliente.getGcarrito().anadirProducto(productoSeleccionado);
                            agregarProductoTablaCarrito(productoSeleccionado);
                            frame.tableProductosCatalogo.setValueAt(frame.tableProductosCatalogo.getValueAt(selectedRow[0], frame.tableProductosCatalogo.getColumnCount()-1),selectedRow[0], frame.tableProductosCatalogo.getColumnCount()-1);
                            setProductos();
                        }else{
                            this.mostrarAviso("Aviso", "Decidete");
                        }
                    }
                }
            }
            frame.tableCarrito.clearSelection();
        }catch(ClassCastException ex){
            this.mostrarAviso("Aviso", "Seleccione la columna ID para agregar al carrito");
        }
    }
    public void agregarProductoTablaCarrito(Producto producto){
        DefaultTableModel yourModel = (DefaultTableModel)  frame.tableCarrito.getModel();
        yourModel.addRow(new Object[]{producto.getIdProducto(), producto.getNombre(), producto.getDescripcion(),producto.getPrecio()});
        mostrarInfoCompra();
    }
    public void quitarProductoTablaCarrito(int selectedRow){
        DefaultTableModel yourModel = (DefaultTableModel)  frame.tableCarrito.getModel();
        yourModel.removeRow(selectedRow);
        mostrarInfoCompra();
    }
    public void mostrarInfoCompra(){
        frame.labelArticulosComprar.setText(Integer.toString(cliente.getGcarrito().getNoArticulos()));
        frame.labelSubtotal.setText(Integer.toString(cliente.getGcarrito().getSubtotal()));
        frame.labelComprar.setText(Double.toString(cliente.getGcarrito().getTotal()));
    }
    public void limpiarCarrito(){
        frame.labelArticulosComprar.setText("");
        frame.labelSubtotal.setText("");
        frame.labelComprar.setText("");
        DefaultTableModel temp;
        try{
            temp = (DefaultTableModel) frame.tableCarrito.getModel();
            int a =temp.getRowCount();
            for(int i=0; i<a; i++)
                temp.removeRow(0);
        }catch(Exception e){
            System.out.println(e);
        }
    }
    public void limpiarCatalogo(){
        DefaultTableModel temp;
        try{
            temp = (DefaultTableModel) frame.tableProductosCatalogo.getModel();
            int a =temp.getRowCount();
            for(int i=0; i<a; i++)
                temp.removeRow(0);
        }catch(Exception e){
            System.out.println(e);
        }
    }
    public void preguntarTicket(){
        int seleccion=JOptionPane.showConfirmDialog(frame, "Desea ver su ticket de compra?","Ticket recibidó",JOptionPane.YES_NO_OPTION);
                //JOptionPane.showInputDialog(frame, "¿Desea ver su Ticket de compra?");
            if(seleccion == JOptionPane.YES_OPTION){
                File file=new File("./tickets/ticket.pdf");
                if(file.exists()){
                    try {
                        Desktop.getDesktop().open(file);
                    } catch (IOException ex) {
                        Logger.getLogger(ControlCliente.class.getName()).log(Level.SEVERE, null, ex);
                        this.mostrarError("Error", "Error al abrir su ticket");
                    }
                }else{
                    this.mostrarError("Error", "Error al abrir su ticket");
                }
            }else{
                this.mostrarAviso("OK", "Ni queria enseñarlo :'v");
            }
    }
    /*****************validaciones*****************************************************/
    private  boolean isNumeric(String cadena){
	try {
		Integer.parseInt(cadena);
		return true;
	} catch (NumberFormatException nfe){
		return false;
        }
    }
}
