package archivos;

import java.io.Serializable;

/**
 *
 * @author jdiaz
 */
public class InfoArchivo implements Serializable{
    private String nombre;
    private String ext;
    private int peso;
    private String tipo;

    public InfoArchivo(String nombre, String tipo,int peso) {
        this.nombre = nombre;
        this.tipo=tipo;
        this.peso=peso;
    }
    public InfoArchivo(String nombre, String tipo) {
        this.nombre = nombre;
        this.tipo=tipo;
    }
    public InfoArchivo(String nombre) {
        this.nombre = nombre;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public void destroy(){
        this.ext=null;
        this.nombre=null;
        this.peso=0;
        System.gc();
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }
    
}
