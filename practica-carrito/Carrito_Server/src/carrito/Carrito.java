/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrito;

import java.io.Serializable;
import java.util.ArrayList;
import productos.Producto;

/**
 *
 * @author Bryan Ramos
 */
public class Carrito implements Serializable{
    private int cantidadArticulos;
    private ArrayList<Producto> productos;
    int subtotal;
    double total;
    
    public Carrito(){
        productos = new ArrayList<>();
    }

    public Carrito(int cantidadArticulos, ArrayList<Producto> productos, int subtotal, double total) {
        this.cantidadArticulos = cantidadArticulos;
        this.productos = productos;
        this.subtotal = subtotal;
        this.total = total;
    }
    
    public int getCantidadArticulos(){
        return cantidadArticulos;
    }

    public void setCantidadArticulos(int cantidadArticulos) {
        this.cantidadArticulos = productos.size();
    }
    

    public ArrayList<Producto> getProductos() {
        return productos;
    }
    public void agregarProducto(Producto producto){
        productos.add(producto);
    }
    
}
