package carrito_server.sistema.compras;

import carrito.Carrito;
import carrito_server.Server;
import carrito_server.sistema.generarPDF.EstructuradorPDF;
import carrito_server.sistema.respuestas.GestionadorRespuestas;
import com.itextpdf.text.DocumentException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import productos.Producto;
import respuestas.Respuesta;

/**
 *
 * @author jdiaz
 */
public class GestionCompra {
    private Server server;

    public GestionCompra(Server server) {
        this.server = server;
    }
    public void guardarCompra(Carrito carrito){
        ArrayList<Producto> productos=carrito.getProductos();
        productos.forEach(p -> {
            try {
                server.getControl().getGproducto().actualizarExistenciaProducto(p);
                System.out.println("Se guarda compra de :" +p.getNombre());
            } catch (SQLException ex) {
                Logger.getLogger(GestionCompra.class.getName()).log(Level.SEVERE, null, ex);
                server.getControl().mostrarError("Error", "Error al actualizar la existencia del producto");
            }
        });
        //se termino de guardar las existencias se debe generar un ticket
        EstructuradorPDF estructurador = new EstructuradorPDF(carrito.getProductos());
        try {
            estructurador.createPdf("./ticket/ticket.pdf");
            File ticket=new File("./ticket/ticket.pdf");
            if(ticket.exists()){
                server.iniciarGestionEnvioPDF(ticket);
                GestionadorRespuestas grespuestas=new GestionadorRespuestas(server);
                grespuestas.hacerRespuesta(new Respuesta("/CompraGuardada"));
            }
        } catch (DocumentException | IOException ex) {
            Logger.getLogger(EstructuradorPDF.class.getName()).log(Level.SEVERE, null, ex);
        } 
        server.terminarGestionCompra();
    }
}
