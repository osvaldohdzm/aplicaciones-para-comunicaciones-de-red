/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrito_server.sistema.generarPDF;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import productos.Producto;

/**
 *
 * @author Bryan Ramos
 */
public class EstructuradorPDF {
 private ArrayList<Producto> productos;
    private final Font HEADER_NORMAL = new Font(FontFamily.HELVETICA, 15, Font.NORMAL, BaseColor.GRAY);
    private final Font DATE_NORMAL = new Font(FontFamily.COURIER, 12, Font.BOLD, BaseColor.RED);
    private PdfPTable productsTable;
    private double subtotalCompra;
    private double totalCompra;
    private int cantidad;
    private String fecha;
    private DateFormat dateFormat;
    private Date date;
    public EstructuradorPDF(){
        productos = null;
        subtotalCompra = 0;
        totalCompra = 0;
        cantidad = 0;
        dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        date = new Date();
        fecha = dateFormat.format(date);
    }
    public EstructuradorPDF(ArrayList<Producto> productos){
        subtotalCompra = 0;
        totalCompra = 0;
        cantidad = 0;
        dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        date = new Date();
        fecha = dateFormat.format(date);
        this.productos = productos;
    }
    public void agregarProductos(ArrayList<Producto> productos){
        this.productos = productos;
    }
    public void createPdf(String filename) throws DocumentException, IOException {
        productsTable = new PdfPTable(5);
        // step 1
        Document document = new Document(PageSize.LETTER);
        document.setMargins(50,50,50,50);
        document.setMarginMirroring(true);
        // step 2
        PdfWriter.getInstance(document, new FileOutputStream(filename));
        // step 3
        document.open();
        // step 4
        DottedLineSeparator dottedline = new DottedLineSeparator();
        dottedline.setOffset(-2);
        dottedline.setGap(2f);
        
        Paragraph header = new Paragraph();
        header.setAlignment(Element.ALIGN_CENTER);
        Chunk headerText = new Chunk("Gracias por tu compra, este es tu comprobante",HEADER_NORMAL);
        header.add(headerText);
        header.add(dottedline);
        header.setSpacingAfter(50);
        document.add(header);
        
        
        Paragraph event_time = new Paragraph();
        event_time.setAlignment(Element.ALIGN_RIGHT);
        event_time.add(new Chunk("Hora y fecha de la compra: " + fecha,DATE_NORMAL));
        event_time.setSpacingAfter(50);
        document.add(event_time);
 
        productsTable.setTotalWidth(new float[]{ 20,120,100,100,70 });
        productsTable.setLockedWidth(true);
        generarEncabezados();
        productos.stream().forEach((producto) -> {
            System.out.println("Producto: " + producto.getDescripcion());
            productsTable.addCell(producto.getIdProducto());
            productsTable.addCell(producto.getNombre());
            productsTable.addCell(producto.getDescripcion());
            productsTable.addCell(producto.getCategoria());
            productsTable.addCell(producto.getPrecio());
            subtotalCompra = subtotalCompra+ Double.parseDouble(producto.getPrecio());
            cantidad++;
        });
        totalCompra = totalCompra + subtotalCompra*1.16;
        agregarPrecios();
        agregarCantidadArticulos();
        document.add(productsTable);     
        // step 5
        document.close();
    }
    private void generarEncabezados(){
        productsTable.addCell("ID");
        productsTable.addCell("Nombre");
        productsTable.addCell("Descripción");
        productsTable.addCell("Categoría");
        productsTable.addCell("Precio");
    }
    private void agregarPrecios(){    
        productsTable.addCell("");
        productsTable.addCell("Subtotal");
        productsTable.addCell(Double.toString(subtotalCompra));
        productsTable.addCell("Total");
        productsTable.addCell(Double.toString(totalCompra));
    }
    private void agregarCantidadArticulos(){
        productsTable.addCell("");
        productsTable.addCell("");
        productsTable.addCell("");
        productsTable.addCell("Cantidad de articulos en el carrito");
        productsTable.addCell(Integer.toString(cantidad));
    }
    
}
