package carrito_server.sistema.esperadores.peticiones;

import carrito.Carrito;
import carrito_server.Server;
import carrito_server.sistema.respuestas.GestionadorRespuestas;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import peticiones.Peticion;
import respuestas.Respuesta;

/**
 *
 * @author jdiaz
 */
public class EsperadorPeticiones implements Runnable{
    private final Server server;
    private Object objeto;
    public EsperadorPeticiones(Server server) {
        this.server = server;
    }
    
    @Override
    public void run() {
       while(true){
           try {
               objeto=(Object)server.getIn().readObject();
               if(objeto instanceof Peticion){
                   System.out.println("Era peticion");
                   Peticion peticion=(Peticion)objeto;
                   analizarPeticion(peticion);
               }
               if(objeto instanceof Carrito){
                   System.out.println("Era un carrito");
                   Carrito carrito=(Carrito)objeto;
                   //Significa que el cliente ha realizado una compra y hay que actualizar el inventario
                   server.iniciarGestionCompra();
                   server.getGcompra().guardarCompra(carrito);
                   
               }
           } catch (IOException | ClassNotFoundException ex) {
               server.getControl().mostrarError("Error", "El cliente se ha desconectado, Peticiones");
               Logger.getLogger(EsperadorPeticiones.class.getName()).log(Level.SEVERE, null, ex);
               server.esperarConexion();
               break;
           }
       }
    }
    public void analizarPeticion(Peticion peticion){
        System.out.println(peticion.getInformacion());
        switch(peticion.getPeticion()){
            case "/ImagenesCatalogo":
                {
                    System.out.println("El cliente solicita las imagenes del catalogo");
                    GestionadorRespuestas grespuestas=new GestionadorRespuestas(server);
                    grespuestas.hacerRespuesta(new Respuesta("/ImagenesCatalogo"));
                    server.iniciarGestionEnvioImagenes();
                }
            break;
            case "/EnviaOtraImagenCatalogo":
                {
                    System.out.println("El cliente solicita el nombre de imagen");
                    if(server.getGenvioimagenes()==null){
                        System.out.println("-------el ultimo--------");
                    }else{
                        server.getGenvioimagenes().setBanderaNombre(true);
                    }
                    
                }
            break;
            case "/EnviaSiguienteImagen":
                {
                    System.out.println("El cliente solicita la imagen");
                    server.getGenvioimagenes().setBanderaImagen(true);
                }
            break;
            case "/ProductosCatalogo":
                {
                    System.out.println("El cliente solicita los productos");
                    GestionadorRespuestas grespuestas=new GestionadorRespuestas(server);
                    grespuestas.hacerRespuesta(new Respuesta("/ProductosCatalogo"));
                    server.iniciarGestionEnvioProductos();
                }
            break;
            case "/EnviaProductos":
                {
                    System.out.println("El cliente solicita los productos, enviandolos");
                    GestionadorRespuestas grespuestas=new GestionadorRespuestas(server);
                    server.getGenvioProductos().setBanderaproductos(true);
                }
            break;
            case "/CategoriasCatalogo":
                {
                    System.out.println("El cliente solicita las categorias de los productos");
                    GestionadorRespuestas grespuestas=new GestionadorRespuestas(server);
                    grespuestas.hacerRespuesta(new Respuesta("/CategoriasCatalogo"));
                    server.iniciarGestionEnvioCategorias();
                }
            break;
            case "/EnviaCategorias":
                {
                    System.out.println("El cliente solicita las  categorias de productos, enviandolas");
                    server.getGenvioCategorias().setBanderaCategorias(true);
                }
            break;
            case "/EnviaInfoTicket":
                {
                    server.getGenvioPDF().setBanderaNombrePDF(true);
                }
            break;
            case "/EnviaTicket":
                {
                    server.getGenvioPDF().setBanderaPDF(true);
                }
            break;
            default:
                {   

                System.out.println("Default");

                }
            break;
        }
    }
}
