/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrito_server.sistema.respuestas;

import carrito_server.Server;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import respuestas.Respuesta;

/**
 *
 * @author jdiaz
 */
public class GestionadorRespuestas {
    private Server server;

    public GestionadorRespuestas(Server server) {
        this.server = server;
    }
    public void hacerRespuesta(Respuesta respuesta){
        try {
            server.getOut().writeObject(respuesta);
            server.getOut().flush();
            System.out.println("Respuesta enviada");
        } catch (IOException ex) {
            Logger.getLogger(GestionadorRespuestas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
