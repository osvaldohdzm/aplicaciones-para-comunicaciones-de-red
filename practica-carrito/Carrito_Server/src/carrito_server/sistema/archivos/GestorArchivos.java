package carrito_server.sistema.archivos;


import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.COPY_ATTRIBUTES;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 *
 * @author jdiaz
 */
public class GestorArchivos {
    public Path copiarArchivo2Raiz(Path fuente) throws IOException{
        String auxdestino = new java.io.File("./img/catalog").getCanonicalPath();
        Path destino=Paths.get(auxdestino, fuente.getFileName().toString());
        File filedestino=new File(destino.toAbsolutePath().toString());
        Files.copy(fuente, destino, REPLACE_EXISTING, COPY_ATTRIBUTES, NOFOLLOW_LINKS);
        return destino;
    }
}
