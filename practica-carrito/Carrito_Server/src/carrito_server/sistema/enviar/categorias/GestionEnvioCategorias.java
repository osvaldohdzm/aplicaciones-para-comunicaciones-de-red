package carrito_server.sistema.enviar.categorias;

import carrito_server.Server;
import categorias.Categoria;
import categorias.Categorias;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jdiaz
 */
public class GestionEnvioCategorias implements Runnable{
    private final Server server;
    private volatile boolean banderaCategorias;
    ArrayList<Categoria> categorias=new ArrayList<>();
    public GestionEnvioCategorias(Server server) {
        this.server = server;
        banderaCategorias=false;
    }
    
    @Override
    public void run() {
        try {
            int enviado=0;
            categorias=server.getControl().getGproducto().getCategoriasClass();
            while(enviado==0){
                if(banderaCategorias==true){
                    enviado=enviarCategorias(categorias);
                    if(enviado==1){
                        break;
                    }
                }
            }
            System.out.println("Categorias enviadas");
            server.terminarGestionEnvioCategorias();
        } catch (SQLException ex) {
            Logger.getLogger(GestionEnvioCategorias.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public int enviarCategorias(ArrayList categorias){
        Categorias classcategorias=new Categorias(categorias);
        try {
            server.getOut().writeObject(classcategorias);
            server.getOut().flush();
            return 1;
        } catch (IOException ex) {
            Logger.getLogger(GestionEnvioCategorias.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public boolean isBanderaCategorias() {
        return banderaCategorias;
    }

    public void setBanderaCategorias(boolean banderaCategorias) {
        this.banderaCategorias = banderaCategorias;
    }
    
    public void destroy(){
        categorias.clear();
        categorias=null;
        System.gc();
    }
}
