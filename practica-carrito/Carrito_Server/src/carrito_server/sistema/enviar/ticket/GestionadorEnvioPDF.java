package carrito_server.sistema.enviar.ticket;

import archivos.InfoArchivo;
import carrito_server.Server;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jdiaz
 */
public class GestionadorEnvioPDF implements Runnable{
    private final Server server;
    private volatile boolean banderaNombrePDF;
    private volatile boolean banderaPDF;
    private File filepdf;
    ArrayList<File> imagenes=new ArrayList<>();
    public GestionadorEnvioPDF(Server server, File pdf) {
        this.server = server;
        banderaNombrePDF=false;
        banderaPDF=false;
        this.filepdf=pdf;
    }
    @Override
    public void run() {
        enviarPDFs();
    }
    public void enviarPDFs(){
        try {
            //for(int i=0;i<imagenes.size();i++){
                System.out.println("enviar imagenes gestiondor");
                while(true){
                    if(banderaNombrePDF==true){
                        System.out.println("nombre");
                        enviarNombrePDF(this.filepdf);
                    }
                    if(banderaPDF==true){
                        System.out.println("imagen");
                        enviarPDF(this.filepdf);
                        break;
                    }
                }
            //}
            System.out.println("Todas los PDF han sido enviados");
            enviarTerminoEnvioPDF();
        } catch (IOException ex) {
            Logger.getLogger(GestionadorEnvioPDF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void enviarNombrePDF(File pdf) throws IOException{
        String nombre=pdf.getName();
        System.out.println("El nombre del pdf es: "+ nombre);
        System.out.println("Su peso es: "+ (int)pdf.length());
        InfoArchivo info=new InfoArchivo(nombre,"pdf",(int)pdf.length());
        server.getOut().writeObject(info);
        server.getOut().flush();
        banderaNombrePDF=false;
        System.out.println("Termino de nombre de pdf enviado");
    }
    private void enviarTerminoEnvioPDF() throws IOException{
        server.getOut().reset();
        InfoArchivo info=new InfoArchivo("/TerminePDF","pdf");
        server.getOut().writeObject(info);
        System.out.println("Bandera se enviaron todas los pdf");
        server.terminarGestionEnvioPDF();
    }
    private void enviarPDF(File pdf) throws IOException{
        OutputStream output = server.getClientSocket().getOutputStream();
        try {
            System.out.println("iniciar proceso de envio de pdf");
            File archivoF=pdf;
            FileInputStream file = new FileInputStream(archivoF.getAbsolutePath());
            byte[] buffer = new byte[(int)pdf.length()];
            int bytesRead = 0;
            while((bytesRead = file.read(buffer))>0){
                output.write(buffer,0,bytesRead);
            }
            output.flush();
            System.out.println("todo el pdf ha sido enviado por el socket");
            //output.close();
            file.close();
            banderaPDF=false;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GestionadorEnvioPDF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isBanderaNombrePDF() {
        return banderaNombrePDF;
    }

    public void setBanderaNombrePDF(boolean banderaNombrePDF) {
        this.banderaNombrePDF = banderaNombrePDF;
    }

    public boolean isBanderaPDF() {
        return banderaPDF;
    }

    public void setBanderaPDF(boolean banderaPDF) {
        this.banderaPDF = banderaPDF;
    }
    
}
