package carrito_server.sistema.enviar.imagenes;

import archivos.InfoArchivo;
import carrito_server.Server;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import productos.Producto;

/**
 *
 * @author jdiaz
 */
public class GestionadorEnvioImagenes implements Runnable{
    private final Server server;
    private volatile boolean banderaNombre;
    private volatile boolean banderaImagen;
    ArrayList<File> imagenes=new ArrayList<>();
    public GestionadorEnvioImagenes(Server server) {
        this.server = server;
        banderaNombre=false;
        banderaImagen=false;
    }
    @Override
    public void run() {
       try {
            ArrayList<Producto> productos=server.getControl().getGproducto().getProductos();
            for(int i=0;i<productos.size();i++){
                File imagen=new File(productos.get(i).getImagen());
                imagenes.add(imagen);
            }
            System.out.println("Imagenes cargadas listas para su envio " + imagenes.size());
            enviarImagenes();
        } catch (SQLException ex) {
            Logger.getLogger(GestionadorEnvioImagenes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void enviarImagenes(){
        try {
            for(int i=0;i<imagenes.size();i++){
                System.out.println("for enviar "+i);
                while(true){
                    if(banderaNombre==true){
                        System.out.println("nombre");
                        enviarNombreImagen(imagenes.get(i));
                    }
                    if(banderaImagen==true){
                        System.out.println("imagen");
                        enviarImagen(imagenes.get(i));
                        break;
                    }
                }
            }
            System.out.println("Todas las imagenes han sido enviadas");
            enviarTerminoEnvioImagen();
        } catch (IOException ex) {
            Logger.getLogger(GestionadorEnvioImagenes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void enviarNombreImagen(File imagen) throws IOException{
        String nombre=imagen.getName();
        System.out.println("El nombre de la imagen es: "+ nombre);
        System.out.println("Su peso es: "+ (int)imagen.length());
        InfoArchivo info=new InfoArchivo(nombre,"imagen",(int)imagen.length());
        server.getOut().writeObject(info);
        server.getOut().flush();
        banderaNombre=false;
        System.out.println("Termino de nombre de imagen enviado");
    }
    private void enviarTerminoEnvioImagen() throws IOException{
        server.getOut().reset();
        InfoArchivo info=new InfoArchivo("/TermineImagenes","imagen");
        server.getOut().writeObject(info);
        System.out.println("Bandera se enviaron todas las imagenes enviada");
        server.terminarGestionEnvioImagenes();
    }
    private void enviarImagen(File imagen) throws IOException{
        OutputStream output = server.getClientSocket().getOutputStream();
        try {
            System.out.println("iniciar proceso de envio de imagen");
            File archivoF=imagen;
            FileInputStream file = new FileInputStream(archivoF.getAbsolutePath());
            byte[] buffer = new byte[(int)imagen.length()];
            int bytesRead = 0;
            while((bytesRead = file.read(buffer))>0){
                output.write(buffer,0,bytesRead);
            }
            output.flush();
            System.out.println("toda la imagen ha sido enviada");
            //output.close();
            file.close();
            banderaImagen=false;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GestionadorEnvioImagenes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void destroy(){
        this.banderaImagen=false;
        this.banderaNombre=false;
        this.imagenes.clear();
        this.imagenes=null;
    }
    public void setBanderaNombre(boolean banderaNombre) {
        System.out.println("envian true nombre");
        this.banderaNombre = banderaNombre;
    }

    public void setBanderaImagen(boolean banderaImagen) {
        System.out.println("envian true imagen");
        this.banderaImagen = banderaImagen;
    }

    public boolean getBanderaNombre() {
        return banderaNombre;
    }

    public boolean getBanderaImagen() {
        return banderaImagen;
    }
}
