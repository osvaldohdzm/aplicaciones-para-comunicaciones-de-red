package carrito_server.sistema.enviar.productos;

import carrito_server.Server;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import productos.Producto;
import productos.Productos;

/**
 *
 * @author jdiaz
 */
public class GestionEnvioProductos implements Runnable{
    private final Server server;
    private volatile boolean banderaproductos;
    ArrayList<Producto> productos=new ArrayList<>();
    public GestionEnvioProductos(Server server) {
        this.server = server;
        banderaproductos=false;
    }
    
    @Override
    public void run() {
        try {
            productos=server.getControl().getGproducto().getProductos();
            int enviado=0;
            while(enviado==0){
                if(banderaproductos==true){
                    enviado=enviarProductos(productos);
                    if(enviado==1){
                        break;
                    }
                }
            }
            System.out.println("Productos enviados");
            server.terminarGestionEnvioProductos();
        } catch (SQLException ex) {
            Logger.getLogger(GestionEnvioProductos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public int enviarProductos(ArrayList productos){
        Productos classproductos=new Productos(productos);
        try {
            server.getOut().writeObject(classproductos);
            server.getOut().flush();
            return 1;
        } catch (IOException ex) {
            Logger.getLogger(GestionEnvioProductos.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }
    public boolean isBanderaproductos() {
        return banderaproductos;
    }

    public void setBanderaproductos(boolean banderaproductos) {
        this.banderaproductos = banderaproductos;
    }
    public void destroy(){
        productos.clear();
        productos=null;
        System.gc();
    }
}
