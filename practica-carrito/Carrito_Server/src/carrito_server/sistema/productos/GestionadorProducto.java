package carrito_server.sistema.productos;

import productos.Producto;
import carrito_server.bd.GestorConexionBD;
import carrito_server.interfaz.control.IServerControl;
import categorias.Categoria;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author jdiaz
 */
public class GestionadorProducto extends GestorConexionBD{
    private Connection conBD;
    private IServerControl control;
    private Statement statement;
    private ResultSet resultset;
    
    
    
    public GestionadorProducto(IServerControl control) {
        try {
            this.control=control;
            conBD=super.getConnection();
            statement = conBD.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        } catch (SQLException ex) {
            control.mostrarError("Conexión BD", "Error al conectarse con la base de datos");
        }
    }
    public void anadirNuevaCategoria(String categoria) throws SQLException{
        statement.execute("INSERT INTO categoria " +  
                "(categoria)VALUES('"+categoria+"')");
        
    }
    public void añadirProducto(String nombre, String descripcion,String categoria,String precio,int existencia,String imagen)throws SQLException{
        int idcategoria=this.getIdCategoria(categoria);
        System.out.println(idcategoria);
        statement.execute("INSERT INTO producto " +  
                "(nombre,descripcion,idcategoria,precio,existencia,imagen)" +
                "VALUES("
                 + "'" + nombre + "',"
                + "'" + descripcion + "',"
                + "" + idcategoria + ","
                + "'" + precio + "',"
                + "" + existencia + ","
                + "'" + imagen + "');" );
    }
    public boolean existeCategoria(String categoria){
        boolean aux=false;
        try {
            resultset=null;
            resultset =  statement.executeQuery("select categoria from categoria where categoria='"+categoria+"'");
            if(resultset.next()){
                aux=true;
            }
            return aux;
        } catch (SQLException ex) {
            System.out.println("Error en existe persona extraviada" + ex);
            return aux;
        }
    }
    public int getIdCategoria(String categoria){
        try {
            resultset=null;
            resultset=statement.executeQuery("select idcategoria from categoria where categoria = "+"'"+categoria+"'");
            if(resultset.next()){
                return Integer.parseInt(resultset.getString("idcategoria"));
            }else{
                return -1;
            }
        } catch (SQLException ex) {
            System.out.println("Error en obtener ID "+ ex);
            return 0;
        }
    }
    public String getCategoria(int idCategoria){
        try {
            resultset=null;
            resultset=statement.executeQuery("select categoria from categoria where idcategoria = "+idCategoria);
            if(resultset.next()){
                return resultset.getString("categoria");
            }else{
                return null;
            }
        } catch (SQLException ex) {
            System.out.println("Error en obtener Categoria "+ ex);
            return null;
        }
    }
    public ArrayList<String> getCategorias() throws SQLException{
        ArrayList<String> categorias =new ArrayList<>();
        resultset=null;
        resultset=statement.executeQuery("select categoria from categoria");
        while(resultset.next()){
            categorias.add(resultset.getString("categoria"));
        }
        return categorias;
    }
    public ArrayList<Categoria> getCategoriasClass() throws SQLException{
        ArrayList<Categoria> categorias =new ArrayList<>();
        resultset=null;
        resultset=statement.executeQuery("select categoria from categoria");
        
        while(resultset.next()){
            categorias.add(new Categoria(resultset.getString("categoria")));
        }
        return categorias;
    }
    public ArrayList<Producto> getProductos() throws SQLException{
        ArrayList<Producto> productos =new ArrayList<>();
        Statement statementaux = conBD.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet resAux=statementaux.executeQuery("select * from producto");
        while(resAux.next()){
            String idProducto = resAux.getString("idProducto");
            String nombre=resAux.getString("nombre");
            String descripcion=resAux.getString("descripcion");
            String categoria=getCategoria(Integer.parseInt(resAux.getString("idcategoria")));
            String precio=resAux.getString("precio");
            int existencia=Integer.parseInt(resAux.getString("existencia"));
            String imagen=resAux.getString("imagen");
            Producto producto=new Producto(idProducto,nombre,descripcion,categoria,precio,existencia,imagen);
            productos.add(producto);
        }
        return productos;
    }
    public ArrayList<Producto> getProductosPorNombre(String nombreProducto) throws SQLException{
        ArrayList<Producto> productos =new ArrayList<>();
        Statement statementaux = conBD.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet resAux=statementaux.executeQuery("select * from producto where nombre like"+"'%"+nombreProducto+"%'");
        while(resAux.next()){
            String idProducto = resAux.getString("idProducto");
            String nombre=resAux.getString("nombre");
            String descripcion=resAux.getString("descripcion");
            String categoria=getCategoria(Integer.parseInt(resAux.getString("idcategoria")));
            String precio=resAux.getString("precio");
            int existencia=Integer.parseInt(resAux.getString("existencia"));
            String imagen=resAux.getString("imagen");
            Producto producto=new Producto(idProducto,nombre,descripcion,categoria,precio,existencia,imagen);
            productos.add(producto);
        }
        return productos;
    }
    public ArrayList<Producto> getProductosPorCategoria(String categoriaProducto) throws SQLException{
        ArrayList<Producto> productos =new ArrayList<>();
        int idCategoria=this.getIdCategoria(categoriaProducto);
        Statement statementaux = conBD.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet resAux=statementaux.executeQuery("select * from producto where idcategoria ="+""+idCategoria+"");
        while(resAux.next()){
            String idProducto = resAux.getString("idProducto");
            String nombre=resAux.getString("nombre");
            String descripcion=resAux.getString("descripcion");
            String categoria=getCategoria(Integer.parseInt(resAux.getString("idcategoria")));
            String precio=resAux.getString("precio");
            int existencia=Integer.parseInt(resAux.getString("existencia"));
            String imagen=resAux.getString("imagen");
            Producto producto=new Producto(idProducto,nombre,descripcion,categoria,precio,existencia,imagen);
            productos.add(producto);
        }
        return productos;
    }
    public Producto getProductoPorID(int idProducto) throws SQLException{
        Producto producto=new Producto();
        Statement statementaux = conBD.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet resAux=statementaux.executeQuery("select * from producto where idproducto ="+""+idProducto+"");
        if(resAux.next()){
            producto.setIdProducto(resAux.getString("idProducto"));
            producto.setNombre(resAux.getString("nombre"));
            producto.setDescripcion(resAux.getString("descripcion"));
            producto.setCategoria(getCategoria(Integer.parseInt(resAux.getString("idcategoria"))));
            producto.setPrecio(resAux.getString("precio"));
            producto.setExistencia(Integer.parseInt(resAux.getString("existencia")));
            producto.setImagen(resAux.getString("imagen"));
        }
        return producto;
    }
    public void borrarProducto(int idProducto) throws SQLException{
        statement.execute("delete from producto where idproducto ="+idProducto+";");
    }
    public void modificarProducto(int idproducto,String nombre, String descripcion,String categoria,String precio,int existencia,String imagen)throws SQLException{
        int idCategoria=this.getIdCategoria(categoria);
        Statement statementaux = conBD.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        String sentencia = "UPDATE producto SET nombre='"+nombre+"', descripcion='"+descripcion+"',idcategoria="+idCategoria+", precio='"+precio+"',existencia="+existencia+",imagen='"+imagen+"' WHERE idproducto="+idproducto+";";
        System.out.println(sentencia);
        statementaux.executeUpdate(sentencia);
    }
    public void actualizarExistenciaProducto(Producto producto)throws SQLException{
        int existencia =producto.getExistencia();
        int idproducto=Integer.parseInt(producto.getIdProducto());
        Statement statementaux = conBD.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        String sentencia = "UPDATE producto SET existencia="+existencia+" WHERE idproducto="+idproducto+";";
        System.out.println(sentencia);
        statementaux.executeUpdate(sentencia);
    }
}
