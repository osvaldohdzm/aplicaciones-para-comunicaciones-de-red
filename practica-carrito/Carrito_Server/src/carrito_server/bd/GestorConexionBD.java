package carrito_server.bd;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jdiaz
 */
public class GestorConexionBD {
    private Connection conBD;
    private ConectorSQL conSQL;
    
    public GestorConexionBD(){
        try {
            iniciarConexion();
            conBD=conSQL.conectar();
        } catch (SQLException ex) {
            Logger.getLogger(GestorConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void iniciarConexion(){
        try {
            conSQL = new ConectorSQL();
        } catch (SQLException | ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            Logger.getLogger(GestorConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public Connection getConnection(){
        return this.conBD;
    }
}
