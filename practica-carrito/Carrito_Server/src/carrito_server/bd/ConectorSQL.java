package carrito_server.bd;

import java.sql.*;

public class ConectorSQL {
    
    private Connection con;
    private final String urlBD;
    private final String contrasenia;
    private final String nombreU;
	
    
    
    public ConectorSQL(String urlBD,String contrasenia,String nombreU) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException{
            String driver = "org.mariadb.jdbc.Driver";
            Class.forName(driver);
            this.urlBD=urlBD;
            this.contrasenia=contrasenia;
            this.nombreU=nombreU;
    }
    
    
    
    public ConectorSQL() throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException{
            String driver = "org.mariadb.jdbc.Driver";
            Class.forName(driver);
            urlBD ="jdbc:mariadb://127.0.0.1:3306/carrito";
            contrasenia="toor";
            nombreU="root";
    }

    public Connection conectar() throws SQLException {
            con = DriverManager.getConnection(urlBD, nombreU, contrasenia);
            System.out.println("Conectado a la BD");
            return con;
    }
}
