package carrito_server;

import carrito_server.conexion.EsperadorConexion;
import carrito_server.interfaz.control.IServerControl;
import carrito_server.sistema.enviar.categorias.GestionEnvioCategorias;
import carrito_server.sistema.compras.GestionCompra;
import carrito_server.sistema.enviar.imagenes.GestionadorEnvioImagenes;
import carrito_server.sistema.enviar.productos.GestionEnvioProductos;
import carrito_server.sistema.enviar.ticket.GestionadorEnvioPDF;
import carrito_server.sistema.esperadores.peticiones.EsperadorPeticiones;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author jdiaz
 */
public class Server{
     private final IServerControl control;
    private ExecutorService esperadorConexion;
    private ExecutorService esperadorPeticiones;
    private ExecutorService esperadorEnvioImagenes;
    private ExecutorService esperadorEnvioProductos;
    private ExecutorService esperadorEnvioPDF;
    private ExecutorService esperadorEnvioCategorias;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private int puerto;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private GestionadorEnvioImagenes genvioimagenes;
    private GestionEnvioProductos genvioProductos;
    private GestionEnvioCategorias genvioCategorias;
    private GestionCompra gcompra;
    private GestionadorEnvioPDF genvioPDF;
    
    
    
    public Server(IServerControl control) {
        this.control=control;
        serverSocket=null;
        clientSocket=null;
        esperadorConexion=null;
        puerto=0;
    }
    public int iniciarServidor(int puerto){
        this.puerto=puerto;
        try {
            serverSocket = new ServerSocket(puerto);// se crea el server
            esperarConexion();
            return 1;
	} catch (IOException e) {
            serverSocket=null;
	    control.mostrarError("Puerto", "Hubo un error al intentar montar el servidor enel puerto especificado");
       	}
            return 0;
    }
    public void cerrarConexion(){
        try {
            if(clientSocket!=null){
                this.clientSocket.close();
                clientSocket=null;
            }
            this.serverSocket.close();
            serverSocket=null;
            esperadorConexion.shutdownNow();
            esperadorConexion=null;
        } catch (IOException ex) {
            control.mostrarError("Conexión", "Hubo un error al intentar cerrar las conexiones");
        }
    }
    public int cerrarServidor() {
        try {
            if (clientSocket != null) {
                this.clientSocket.close();
                clientSocket = null;
            }
            this.serverSocket.close();
            serverSocket = null;
            esperadorConexion.shutdownNow();
            esperadorConexion = null;
            System.gc();
            return 1;
        } catch (IOException ex) {
            return 0;
        }
    }
    public void iniciarGestionCompra(){
        gcompra=new GestionCompra(this);
    }
    public void esperarConexion(){
        esperadorConexion = Executors.newCachedThreadPool();
        esperadorConexion.execute(new EsperadorConexion(this));
    }
    public void terminarEsperaConexion(){
        esperadorConexion.shutdown();
        esperadorConexion=null;
        System.gc();
    }
    public void esperarPeticiones(){
        esperadorPeticiones = Executors.newCachedThreadPool();
        esperadorPeticiones.execute(new EsperadorPeticiones(this));
    }
    public void terminarEsperaPeticiones(){
        esperadorPeticiones.shutdown();
        esperadorPeticiones=null;
        System.gc();
    }
    public void iniciarGestionEnvioImagenes(){
        genvioimagenes=new GestionadorEnvioImagenes(this);
        esperadorEnvioImagenes = Executors.newCachedThreadPool();
        esperadorEnvioImagenes.execute(genvioimagenes);
    }
    public void terminarGestionEnvioImagenes(){
        esperadorEnvioImagenes.shutdownNow();
        esperadorEnvioImagenes=null;
        genvioimagenes.destroy();
        genvioimagenes=null;
        System.gc();
    }
    public void iniciarGestionEnvioProductos(){
        genvioProductos=new GestionEnvioProductos(this);
        esperadorEnvioProductos = Executors.newCachedThreadPool();
        esperadorEnvioProductos.execute(genvioProductos);
    }
    public void terminarGestionCompra(){
        this.gcompra=null;
    }
    public void terminarGestionEnvioProductos(){
        esperadorEnvioProductos.shutdownNow();
        esperadorEnvioProductos=null;
        genvioProductos.destroy();
        genvioProductos=null;
        System.gc();
    }
    public void iniciarGestionEnvioPDF(File pdf){
        genvioPDF=new GestionadorEnvioPDF(this,pdf);
        esperadorEnvioPDF = Executors.newCachedThreadPool();
        esperadorEnvioPDF.execute(genvioPDF);
    }
    public void terminarGestionEnvioPDF(){
        esperadorEnvioPDF.shutdownNow();
        esperadorEnvioPDF=null;
        //genvioPDF.destroy();
        genvioPDF=null;
        System.gc();
    }
    public void iniciarGestionEnvioCategorias(){
        genvioCategorias=new GestionEnvioCategorias(this);
        esperadorEnvioCategorias = Executors.newCachedThreadPool();
        esperadorEnvioCategorias.execute(genvioCategorias);
    }
    public void terminarGestionEnvioCategorias(){
        esperadorEnvioCategorias.shutdownNow();
        esperadorEnvioCategorias=null;
        genvioCategorias.destroy();
        genvioCategorias=null;
        System.gc();
    }
    public Socket getClientSocket() {
        return clientSocket;
    }

    public void setClientSocket(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public void setServerSocket(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    public IServerControl getControl() {
        return control;
    }  

    public ObjectOutputStream getOut() {
        return out;
    }

    public void setOut(ObjectOutputStream out) {
        this.out = out;
    }

    public ObjectInputStream getIn() {
        return in;
    }

    public void setIn(ObjectInputStream in) {
        this.in = in;
    }

    public GestionadorEnvioImagenes getGenvioimagenes() {
        return genvioimagenes;
    }

    public GestionEnvioProductos getGenvioProductos() {
        return genvioProductos;
    }

    public GestionEnvioCategorias getGenvioCategorias() {
        return genvioCategorias;
    }

    public void setGenvioCategorias(GestionEnvioCategorias genvioCategorias) {
        this.genvioCategorias = genvioCategorias;
    }

    public GestionCompra getGcompra() {
        return gcompra;
    }

    public GestionadorEnvioPDF getGenvioPDF() {
        return genvioPDF;
    }
    
}
