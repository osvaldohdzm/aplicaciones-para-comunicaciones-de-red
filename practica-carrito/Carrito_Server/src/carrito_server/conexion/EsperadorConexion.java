package carrito_server.conexion;

import carrito_server.Server;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author jdiaz
 */
public class EsperadorConexion implements Runnable{
    private Socket clientSocket;
    private final Server server;
    private String fecha;
    
    
    public EsperadorConexion(Server server){
        this.server=server;
        DateFormat formater = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        fecha = formater.format(date);
    }
    
    @Override
    public void run() {
        try {
            clientSocket = server.getServerSocket().accept();
            server.setClientSocket(clientSocket);
            ObjectOutputStream out= new ObjectOutputStream(clientSocket.getOutputStream());
            ObjectInputStream in= new ObjectInputStream(clientSocket.getInputStream());
            server.setIn(in);
            server.setOut(out);
            server.esperarPeticiones();
            System.out.println("Esperador de peticiones iniciado");
            server.getControl().mostrarMensajeLog("\n"+"Se conectó un cliente " + fecha +" " + "IP Cliente: " + clientSocket.getInetAddress());
            server.getControl().mostrarAviso("Conectado", "El cliente se ha conectado");   
            server.terminarEsperaConexion();
        } catch (IOException ex) {
            clientSocket=null;
            server.getControl().mostrarError("Conexión", "Error al acpetar conexión del cliente");
        }
    }
}
