package carrito_server.interfaz.control;

import carrito_server.interfaz.IServer;
import carrito_server.interfaz.IconCellRenderer;
import carrito_server.sistema.productos.GestionadorProducto;
import productos.Producto;
import carrito_server.Server;
import carrito_server.sistema.archivos.GestorArchivos;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author ohernandez
 */
public class IServerControl implements ActionListener,KeyListener{
    private final IServer frame;
    private Server server;
    private GestionadorProducto gproducto;
    private GestorArchivos garchivos;
    
    public IServerControl(IServer frame) {
        this.frame=frame;
        server=null;
        gproducto=new GestionadorProducto(this);
        garchivos=new GestorArchivos();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "serverOn":
                {
                    server=new Server(this);
                    int puerto=Integer.parseInt(JOptionPane.showInputDialog(frame, "Ingresa el puerto","8082"));
                    int error=server.iniciarServidor(puerto);
                    if(error==1){
                        this.mostrarAviso("Servidor Iniciado", "El servidor se ha iniciado en el puerto " + puerto);
                        frame.buttonserverOn.setEnabled(false);
                        frame.buttonServerOff.setEnabled(true);
                        frame.buttonServerCatalog.setEnabled(true);
                        this.mostrarMensajeLog("El servidor se ha iniciado en el puerto " + puerto);
                    }else{
                        this.mostrarAviso("Intentelo de nuevo", "Verifique los puertos he intentelo de nueva cuenta");
                    }
                }
            break;
            case "serverOff":
                {
                    int error=server.cerrarServidor();
                    if(error==1){
                        this.mostrarAviso("Conexión terminada", "El servidor se ha cerrado correctamente");
                        frame.buttonServerOff.setEnabled(false);
                        frame.buttonserverOn.setEnabled(true);
                        frame.buttonServerCatalog.setEnabled(false);
                    }else{
                        this.mostrarAviso("Intentelo de nuevo", "El servidor no se ha cerrado correctamente");
                    }
                }
            break;
            case "buttonAddCategoria":
                {
                    String categoria=frame.fieldAddCategoria.getText();
                    if(!categoria.isEmpty()){
                        try {
                            if(!gproducto.existeCategoria(categoria)){
                                gproducto.anadirNuevaCategoria(categoria);
                                this.mostrarAviso("OK", "Categoria creada");
                                frame.fieldAddCategoria.setEnabled(false);
                                frame.buttonAddCategoria.setEnabled(false);
                                frame.comboAddCategoria.addItem(categoria);
                            }else{
                                this.mostrarAviso("Atención", "La categoria ya existe");
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                            this.mostrarError("Insertar", "Error al crear nueva categoria");
                        }
                    }
                }
            break;
            case "buttonAddProducto":
                //producto
                {
                    String nombre=frame.fieldAddNombre.getText();
                    String precio=frame.fieldAddPrecio.getText();
                    String descripcion=frame.fieldAddDescripcion.getText();
                    String categoria=frame.comboAddCategoria.getSelectedItem().toString();
                    int existencia=Integer.parseInt(frame.fieldAddExistencia.getText());
                    String ruta=frame.labelAddRutaImagen.getText();
                    String rutaCompleta=ruta.replace("\\", "\\\\");
                    if(!nombre.isEmpty()&&!precio.isEmpty()&&!descripcion.isEmpty()&&!categoria.isEmpty()&&!ruta.isEmpty()){
                        try {
                            gproducto.añadirProducto(nombre, descripcion, categoria, precio, existencia, rutaCompleta);
                            this.mostrarAviso("Añadido", "El producto ha sido añadido");
                            limpiarAddProducto();
                        } catch (SQLException ex) {
                            Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                            this.mostrarError("Insertar", "Error al crear el nuevo producto");
                        }
                    }else{
                        this.mostrarAviso("Campos Incompletos", "Llene adecuadamente todos los campos e intentelo de nuevo");
                    }
                }
            break;
            case "buttonAddProduct":
                {
                    try {
                        ArrayList<String> categorias=gproducto.getCategorias();
                        String aux[]=new String[categorias.size()];
                        for(int i=0;i<categorias.size();i++){
                            aux[i]=categorias.get(i);
                        }
                        DefaultComboBoxModel model = new DefaultComboBoxModel(aux);//Recibe array
                        frame.comboAddCategoria.setModel(model);
                    } catch (SQLException ex) {
                        Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                        this.mostrarError("Error", "Hubo un problema al obtener las categorias de producto existentes");
                    }
                }
            break;
            case "buttonAddImage":
                {
                    JFileChooser chooser = new JFileChooser();
                    chooser.showOpenDialog(frame);
                    File file = chooser.getSelectedFile();
                    Path filepath= Paths.get(file.toPath().getParent().toString(), file.getName());
                    Path fileDestino;
                    try {
                        fileDestino=garchivos.copiarArchivo2Raiz(filepath);
                        frame.labelAddRutaImagen.setText(fileDestino.toAbsolutePath().toString());
                        Image bi = ImageIO.read(file);
                        frame.labelAddImage.setIcon(new ImageIcon(bi.getScaledInstance(195, 186, Image.SCALE_DEFAULT)));
                    } catch (IOException ex) {
                        Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            break;
            case "buttonCatalog":
                {
                    try {
                            ArrayList<String> categorias=gproducto.getCategorias();
                            String aux[]=new String[categorias.size()];
                            for(int i=0;i<categorias.size();i++){
                                aux[i]=categorias.get(i);
                            }
                            DefaultComboBoxModel model = new DefaultComboBoxModel(aux);//Recibe array
                            frame.comboLookCategoria.setModel(model);
                        } catch (SQLException ex) {
                            Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                            this.mostrarError("Error", "Hubo un problema al obtener las categorias de producto existentes");
                        }
                }
            break;
            case "buttonModifyProduct":
                {
                    try {
                        ArrayList<String> categorias=gproducto.getCategorias();
                        String aux[]=new String[categorias.size()];
                        for(int i=0;i<categorias.size();i++){
                            aux[i]=categorias.get(i);
                        }
                        DefaultComboBoxModel model = new DefaultComboBoxModel(aux);//Recibe array
                        frame.comboModifyCategoria.setModel(model);
                    } catch (SQLException ex) {
                        Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                        this.mostrarError("Error", "Hubo un problema al obtener las categorias de producto existentes");
                    }
                }
            break;
            case "buttonServerCatalog":
                {
                    try {
                        ArrayList<String> categorias=gproducto.getCategorias();
                        String aux[]=new String[categorias.size()];
                        for(int i=0;i<categorias.size();i++){
                            aux[i]=categorias.get(i);
                        }
                        DefaultComboBoxModel model = new DefaultComboBoxModel(aux);//Recibe array
                        frame.comboLookCategoria.setModel(model);
                    } catch (SQLException ex) {
                        Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                        this.mostrarError("Error", "Hubo un problema al obtener las categorias de producto existentes");
                    }
                }
            break;
            case "buttonLookTodos":
                {
                    //Se cargan las columnas para la tabla
                    DefaultTableModel datos = new DefaultTableModel();
                    datos.addColumn("ID");
                    datos.addColumn("Nombre");
                    datos.addColumn("Categoria");
                    datos.addColumn("Precio");
                    datos.addColumn("Existencia");
                    try {
                        //Consultamos todos los productos
                        ArrayList<Producto> productos=gproducto.getProductos();
                        frame.tableLookTodos.setDefaultRenderer(Object.class,new IconCellRenderer());
                        //llenar tabla---------------------------------
                        int noproductos=productos.size();
                        
                        for(int i=0;i<noproductos;i++){
                            Object [] filaTabla = new Object[5];
                            filaTabla[0]=productos.get(i).getIdProducto();//ID
                            filaTabla[1]=productos.get(i).getNombre();//Nombre
                            filaTabla[2]=productos.get(i).getCategoria();//Categoria
                            filaTabla[3]=productos.get(i).getPrecio();//Precio
                            filaTabla[4]=productos.get(i).getExistencia();//Existencia
                            datos.addRow(filaTabla);
                        }
                        frame.tableLookTodos.setModel(datos);
                        this.resizeColumnWidth(frame.tableLookTodos);
                        } catch (SQLException ex) {
                            Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                            this.mostrarError("Error", "Error al cargar los productos");
                        }
                }
            break;
            case "comboLookCategoria":
                {
                    //Se cargan las columnas para la tabla
                    DefaultTableModel datos = new DefaultTableModel();
                    datos.addColumn("ID");
                    datos.addColumn("Nombre");
                    datos.addColumn("Categoria");
                    datos.addColumn("Precio");
                    datos.addColumn("Existencia");
                    try {
                        //Consultamos todos los productos
                        ArrayList<Producto> productos=gproducto.getProductosPorCategoria(frame.comboLookCategoria.getSelectedItem().toString());
                        frame.tableLookTodos.setDefaultRenderer(Object.class,new IconCellRenderer());
                        //llenar tabla---------------------------------
                        int noproductos=productos.size();
                        
                        for(int i=0;i<noproductos;i++){
                            Object [] filaTabla = new Object[5];
                            filaTabla[0]=productos.get(i).getIdProducto();//ID
                            filaTabla[1]=productos.get(i).getNombre();//Nombre
                            filaTabla[2]=productos.get(i).getCategoria();//Categoria
                            filaTabla[3]=productos.get(i).getPrecio();//Precio
                            filaTabla[4]=productos.get(i).getExistencia();//Existencia
                            datos.addRow(filaTabla);
                        }
                        frame.tableLookTodos.setModel(datos);
                        this.resizeColumnWidth(frame.tableLookTodos);
                        } catch (SQLException ex) {
                            Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                            this.mostrarError("Error", "Error al cargar los productos");
                        }
                }
            break;
            case "buttonLookProducto":
                {
                    try {
                        Producto producto=gproducto.getProductoPorID(Integer.parseInt(frame.fieldIDBuscarProducto.getText()));
                        frame.labelLookNombre.setText(producto.getNombre());
                        frame.labelLookID.setText(producto.getIdProducto());
                        frame.areaLookDescripcion.append(producto.getDescripcion());
                        frame.labelLookPrecio.setText(producto.getPrecio());
                        frame.labelLookCategoria.setText(producto.getCategoria());
                        frame.labelLookExistencia.setText(Integer.toString(producto.getExistencia()));
                        File imagen = new File(producto.getImagen());
                        Image bi;
                        try {
                            bi = ImageIO.read(imagen);
                            frame.labelLookImage.setIcon(new ImageIcon(bi.getScaledInstance(195, 186, Image.SCALE_DEFAULT)));
                        } catch (IOException ex) {
                            Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                            this.mostrarError("Error", "No se encontro la imagen del producto");
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                        this.mostrarError("Error", "Error al obtener el producto");
                    }
                }
            break;
            case "buttonDeleteProductoBuscar":
                {
                    try {
                        Producto producto=gproducto.getProductoPorID(Integer.parseInt(frame.labelDeleteIDProducto.getText()));
                        frame.labelDeteleNonbre.setText(producto.getNombre());
                        frame.labelDeleteExistencia.setText(Integer.toString(producto.getExistencia()));
                        System.out.println(producto.getImagen());
                        File imagen = new File(producto.getImagen());
                        Image bi;
                        try {
                            bi = ImageIO.read(imagen);
                            frame.labelDeleteImage.setIcon(new ImageIcon(bi.getScaledInstance(195, 186, Image.SCALE_DEFAULT)));
                        } catch (IOException ex) {
                            Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                            this.mostrarError("Error", "No se encontro la imagen del producto");
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                        this.mostrarError("Error", "Error al obtener el producto");
                    }
                }
            break;
            case "buttonDeleteProducto":
                {
                    int idProducto=Integer.parseInt(frame.labelDeleteIDProducto.getText());
                    String respuesta=JOptionPane.showInputDialog(frame, "¿Esta seguro de borrar el producto?");
                    if(respuesta.equals("si")){
                        try {
                            gproducto.borrarProducto(idProducto);
                            this.mostrarAviso("OK", "Producto Eliminado");
                            frame.labelDeteleNonbre.setText("");
                            frame.labelDeleteExistencia.setText("");
                            ImageIcon icon = new ImageIcon("./img/catalog/default.png");
                            frame.labelDeleteImage.setIcon(icon);
                        } catch (SQLException ex) {
                            Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                            this.mostrarError("Error", "El Producto ha sido eliminado");
                        }
                    }
                }
            break;
            case "buttonBuscarProducto":
                {
                    int idProducto=Integer.parseInt(frame.fieldIDModificarProducto.getText());
                    try {
                        Producto producto=gproducto.getProductoPorID(idProducto);
                        this.mostrarAviso("OK", "Producto Encontrado");
                        frame.fieldModifyCategoria.setEnabled(true);
                        frame.buttonModifyCategoria.setEnabled(true);
                        frame.buttonModifyProducto.setEnabled(true);
                        frame.labelModifyIDProducto.setVisible(false);
                        frame.fieldModifyNombre.setText(producto.getNombre());
                        frame.labelLookID.setText(producto.getIdProducto());
                        frame.fieldModifyDescripcion.setText(producto.getDescripcion());
                        frame.fieldModifyPrecio.setText(producto.getPrecio());
                        frame.comboModifyCategoria.setSelectedItem(gproducto.getIdCategoria(producto.getCategoria()));
                        frame.fieldModifyExistencia.setText(Integer.toString(producto.getExistencia()));
                        frame.labelModifyRutaImagen.setVisible(false);
                        frame.labelModifyRutaImagen.setText(producto.getImagen());
                        frame.labelModifyIDProducto.setText(producto.getIdProducto());
                        File imagen = new File(producto.getImagen());
                        Image bi;
                        try {
                            bi = ImageIO.read(imagen);
                            frame.labelModifyImage.setIcon(new ImageIcon(bi.getScaledInstance(195, 186, Image.SCALE_DEFAULT)));
                        } catch (IOException ex) {
                            Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                            this.mostrarError("Error", "No se encontro la imagen del producto");
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                        frame.buttonModifyProducto.setEnabled(false);
                        this.mostrarError("Error", "Error al recuperar los datos el producto");
                    }
                }
            break;
            case "buttonModifyAddCategoria":
                {
                    String categoria=frame.fieldModifyCategoria.getText();
                    if(!categoria.isEmpty()){
                        try {
                            if(!gproducto.existeCategoria(categoria)){
                                gproducto.anadirNuevaCategoria(categoria);
                                this.mostrarAviso("OK", "Categoria creada");
                                frame.fieldModifyCategoria.setEnabled(false);
                                frame.buttonModifyCategoria.setEnabled(false);
                                frame.comboModifyCategoria.addItem(categoria);
                            }else{
                                this.mostrarAviso("Atención", "La categoria ya existe");
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                            this.mostrarError("Insertar", "Error al crear nueva categoria");
                        }
                    }
                }
            break;
            case "buttonModifyProducto":
                //producto
                {
                    int idProducto=Integer.parseInt(frame.labelModifyIDProducto.getText());
                    String nombre=frame.fieldModifyNombre.getText();
                    String precio=frame.fieldModifyPrecio.getText();
                    String descripcion=frame.fieldModifyDescripcion.getText();
                    String categoria=frame.comboModifyCategoria.getSelectedItem().toString();
                    int existencia=Integer.parseInt(frame.fieldModifyExistencia.getText());
                    String ruta=frame.labelModifyRutaImagen.getText();
                    String rutaCompleta=ruta.replace("\\", "\\\\");
                    if(!nombre.isEmpty()&&!precio.isEmpty()&&!descripcion.isEmpty()&&!categoria.isEmpty()&&!ruta.isEmpty()){
                        try {
                            gproducto.modificarProducto(idProducto,nombre, descripcion, categoria, precio, existencia, rutaCompleta);
                            this.mostrarAviso("OK", "El producto ha sido Modificado");
                            limpiarAddProducto();
                        } catch (SQLException ex) {
                            Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                            this.mostrarError("Error", "Error al modificar el producto");
                        }
                    }else{
                        this.mostrarAviso("Campos Incompletos", "Llene adecuadamente todos los campos e intentelo de nuevo");
                    }
                }
            break;
            case "buttonModifyImage":
                {
                    JFileChooser chooser = new JFileChooser();
                    chooser.showOpenDialog(frame);
                    File file = chooser.getSelectedFile();
                    Path filepath= Paths.get(file.toPath().getParent().toString(), file.getName());
                    Path fileDestino;
                    try {
                        fileDestino=garchivos.copiarArchivo2Raiz(filepath);
                        frame.labelModifyRutaImagen.setText(fileDestino.toAbsolutePath().toString());
                        Image bi = ImageIO.read(file);
                        frame.labelModifyImage.setIcon(new ImageIcon(bi.getScaledInstance(195, 186, Image.SCALE_DEFAULT)));
                    } catch (IOException ex) {
                        Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            break;
        }
    }
    public void mostrarMensajeLog(String msg){
        frame.areaServerLog.append(msg);
    }
    public void limpiarLog(String msg){
        frame.areaServerLog.setText("");
    }
    public void mostrarError(String titulo,String error){
        JOptionPane.showMessageDialog(frame,error,titulo,JOptionPane.ERROR_MESSAGE);
    }
    public void mostrarAviso(String titulo,String aviso){
        JOptionPane.showMessageDialog(frame,aviso,titulo,JOptionPane.INFORMATION_MESSAGE);
    } 
    public Server getServer() {
        return server;
    }
    public void setServer(Server server) {
        this.server = server;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        switch(e.getComponent().getName()){
           case "fiieldLookNombre":
                {
                    //Se cargan las columnas para la tabla
                    DefaultTableModel datos = new DefaultTableModel();
                    datos.addColumn("ID");
                    datos.addColumn("Nombre");
                    datos.addColumn("Categoria");
                    datos.addColumn("Precio");
                    datos.addColumn("Existencia");
                    try {
                        //Consultamos todos los productos
                        ArrayList<Producto> productos=gproducto.getProductosPorNombre(frame.fiieldLookNombre.getText());
                        frame.tableLookTodos.setDefaultRenderer(Object.class,new IconCellRenderer());
                        //llenar tabla---------------------------------
                        int noproductos=productos.size();
                        
                        for(int i=0;i<noproductos;i++){
                            Object [] filaTabla = new Object[5];
                            filaTabla[0]=productos.get(i).getIdProducto();//ID
                            filaTabla[1]=productos.get(i).getNombre();//Nombre
                            filaTabla[2]=productos.get(i).getCategoria();//Categoria
                            filaTabla[3]=productos.get(i).getPrecio();//Precio
                            filaTabla[4]=productos.get(i).getExistencia();//Existencia
                            datos.addRow(filaTabla);
                        }
                        frame.tableLookTodos.setModel(datos);
                        this.resizeColumnWidth(frame.tableLookTodos);
                        } catch (SQLException ex) {
                            Logger.getLogger(IServerControl.class.getName()).log(Level.SEVERE, null, ex);
                            this.mostrarError("Error", "Error al cargar los productos");
                        }
                }
           break;
       }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }
    public void limpiarAddProducto(){
        frame.fieldAddNombre.setText("");
        frame.fieldAddPrecio.setText("");
        frame.fieldAddDescripcion.setText("");
        frame.fieldAddExistencia.setText("");
        frame.labelAddRutaImagen.setText("");
        ImageIcon icon = new ImageIcon("./img/catalog/default.png");
        frame.labelAddImage.setIcon(icon);
    }
     /*---------------------------------------------------*/
    private void resizeColumnWidth(JTable table) {
    final TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            int width = 50; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width +1 , width);
            }
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }
    /*****************validaciones*****************************************************/
    private  boolean isNumeric(String cadena){
	try {
		Integer.parseInt(cadena);
		return true;
	} catch (NumberFormatException nfe){
		return false;
        }
    }

    public GestionadorProducto getGproducto() {
        return gproducto;
    }

    public GestorArchivos getGarchivos() {
        return garchivos;
    }
    
}
