-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: carrito
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (2,'Celulares'),(3,'Accesorios para celular'),(4,'Laptos Gaming'),(5,'Libros'),(6,'Dulces'),(7,'Ventiladores'),(8,'Plumas');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `idproducto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(400) DEFAULT NULL,
  `idcategoria` int(11) DEFAULT NULL,
  `precio` varchar(45) DEFAULT NULL,
  `existencia` int(11) DEFAULT NULL,
  `imagen` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idproducto`),
  KEY `categoria_idx` (`idcategoria`),
  CONSTRAINT `categoria` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (4,'Moto G5','Esta Bien Chido',2,'5499',4,'Z:\\JDIAZRDGZ\\Data\\Github\\AplicacionesEnRed\\Practica2_Carrito\\Carrito_Server\\img\\catalog\\motoG5.jpg'),(5,'S8','Lo mas nuevo de samsung',2,'20000',15,'Z:\\JDIAZRDGZ\\Data\\Github\\AplicacionesEnRed\\Practica2_Carrito\\Carrito_Server\\img\\catalog\\galaxy-s8-s8-plus-together-6.jpg'),(6,'Moto Z Mods Triple Pack','Toda la familia de mods hechos para el Moto Z en un mismo paquete',3,'9000',22,'Z:\\JDIAZRDGZ\\Data\\Github\\AplicacionesEnRed\\Practica2_Carrito\\Carrito_Server\\img\\catalog\\Carousel-Image-Desktop-2x-3rdParty-US-91216-D-jyHWs4gf (1).png'),(7,'Moto Z','La gama mas alta de Lenovo',2,'15000',42,'Z:\\JDIAZRDGZ\\Data\\Github\\AplicacionesEnRed\\Practica2_Carrito\\Carrito_Server\\img\\catalog\\moto-z-1000-QxYhJi7GcD.png'),(13,'1Q84','Un libro, una historia de dos lunas',5,'100',83,'Z:\\JDIAZRDGZ\\Data\\Github\\AplicacionesEnRed\\Practica2_Carrito\\Carrito_Server\\img\\catalog\\220px-1Q84bookcover.jpg'),(14,'Bote Miguelito','Pica Rico ',6,'59',66,'Z:\\JDIAZRDGZ\\Data\\Github\\AplicacionesEnRed\\Practica2_Carrito\\Carrito_Server\\img\\catalog\\2945.png'),(15,'Ventilador Chido','Es chino',7,'2999',0,'Z:\\JDIAZRDGZ\\Data\\Github\\AplicacionesEnRed\\Practica2_Carrito\\Carrito_Server\\img\\catalog\\D_Q_NP_729011-MLA20456841970_102015-H.jpg'),(16,'Pluma','Kilometricas',8,'5',37,'Z:\\JDIAZRDGZ\\Data\\Github\\AplicacionesEnRed\\Practica2_Carrito\\Carrito_Server\\img\\catalog\\descarga.gif');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-23 10:21:49
