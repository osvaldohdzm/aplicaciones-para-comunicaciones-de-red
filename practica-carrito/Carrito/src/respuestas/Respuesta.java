
package respuestas;

import java.io.Serializable;

/**
 *
 * @author jdiaz
 */
public class Respuesta implements Serializable{
    private String respuesta;

    public Respuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getRespuesta() {
        return respuesta;
    }
    
}
