package productos;

import java.io.Serializable;

/**
 *
 * @author jdiaz
 */
public class Producto implements Serializable{
    private String idProducto;
    private String nombre;
    private String Descripcion;
    private String categoria;
    private String precio;
    private int existencia;
    private String imagen;

    public Producto(String idProducto,String nombre, String Descripcion, String categoria, String precio, int existencia, String imagen) {
        this.idProducto=idProducto;
        this.nombre = nombre;
        this.Descripcion = Descripcion;
        this.categoria = categoria;
        this.precio = precio;
        this.existencia = existencia;
        this.imagen = imagen;
    }
    public Producto(){
        
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public int getExistencia() {
        return existencia;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }
    @Override
    public String toString(){
        return "id: " + idProducto + " descripción: " + this.Descripcion;
    }
    
}
