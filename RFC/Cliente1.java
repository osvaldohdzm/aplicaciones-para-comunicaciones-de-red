import java.net.*;
import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;

public class Cliente1 {


public static String primerosCuatroCaracteresRFC(String nombre, String apellidoPaterno, String apellidoMaterno)
{
    //Eliminar acentos y llevar a mayúsculas
    nombre = eliminarAcentosYSimbolos(nombre);
    apellidoPaterno = eliminarAcentosYSimbolos(apellidoPaterno);
    apellidoMaterno = eliminarAcentosYSimbolos(apellidoMaterno);

    //Nombre: Omitir palabras que no se utilizan, MARIA, JOSE y compuestos, y obtener las 2 primeras letras
    Pattern pattern = Pattern.compile("\\A(?:(?:MARIA|JOSE) )?+(?:(?:DEL?|L(?:AS?|OS)|M(?:AC|[CI])|V[AO]N|Y)\\b ?)*+([A-Z&]?)([A-Z&]?)");
    final Matcher matcherNom = pattern.matcher(nombre);
    matcherNom.find();

    //Apellido: Omitir palabras que no se utilizan, y obtener la primera letra y la vocal interna (si el apellido tiene más de 2 letras)
    pattern = Pattern.compile("\\A(?:(?:DEL?|L(?:AS?|OS)|M(?:AC|[CI])|V[AO]N|Y)\\b ?)*+(([A-Z&]?)[B-DF-HJ-NP-TV-Z&]*([AEIOU]?)[A-Z&]?)");
    final Matcher matcherPat = pattern.matcher(apellidoPaterno);
    matcherPat.find();

    final Matcher matcherMat = pattern.matcher(apellidoMaterno);
    matcherMat.find();

    //LETRAS
    //Obtener vocal de apellido paterno y letra(s) del nombre
    String letraPat = matcherPat.group(2);
    String letraMat = matcherMat.group(2);
    String letraNom = matcherNom.group(1);
    String rfc;
    if (letraPat.isEmpty() || letraMat.isEmpty()) {
        //Si no tiene alguno de los apellidos (paterno o materno), se toma la primera y segunda letra del apellido que tiene
        //y el 4to caracter será la segunda letra del nombre.
        rfc = (matcherPat.group(1) + matcherMat.group(1)).substring(0,2) + letraNom + matcherNom.group(2);
    }
    else if (matcherPat.group(1).length() > 2)
    {
        String vocal = matcherPat.group(3);
        //Cuando el apellido paterno no tiene vocales, se utiliza una X.
        if (vocal.isEmpty())
            vocal = "X";
        rfc = letraPat + vocal + letraMat + letraNom;
    } 
    else
    {
        //Si el apellido paterno tiene 1 o 2 letras, no se toma la primera vocal,
        //y el 4to caracter es la segunda letra del nombre.
        rfc = letraPat + letraMat + letraNom + matcherNom.group(2);
    }


    //Cuando las 4 letras resulten en una palabra inconveniente (regla 9), se modifica la última letra a una X
    if (rfc.matches("BUE[IY]|C(?:A[CGK][AO]|O(?:GE|J[AEIO])|ULO)|FETO|GUEY|JOTO|K(?:A(?:[CG][AO]|KA)|O(?:GE|JO)|ULO)|M(?:AM[EO]|E(?:A[RS]|ON)|ION|OCO|ULA)|P(?:E(?:D[AO]|NE)|UT[AO])|QULO|R(?:ATA|UIN)"))
        return rfc.substring(0,3) + "X";
    else
        return rfc;
}

public static String eliminarAcentosYSimbolos(String s)
{
    s = Normalizer.normalize(s.replaceAll("[Ññ]","&"), Normalizer.Form.NFD);
    s = s.replaceAll("[^&A-Za-z ]", "");
    return s.trim().toUpperCase();
}


    public static void main(String[] args) {
        String nombre="", apellidoPaterno="",  apellidoMaterno="", fecha=""; 
        

        try {
            // Crea un socket de datagrama y asigna puerta a la computadora local
            DatagramSocket s = new DatagramSocket(2000);
            // Espera el mensaje del cliente
            System.out.println("Servidor iniciado, esperando cliente");
            DatagramPacket p = new DatagramPacket(new byte[200], 200);
            for (int i=0;i<=4;i++) {              

                if(i== 0)
                {
                // Creado para recibir paquetes de longitud lenght
                p = new DatagramPacket(new byte[200], 200);
                s.receive(p); // Recibe el DatagramPacket p
                // Agrega la dirección y el puerto de donde es enviado p
                System.out.println("Datagrama recibido desde " + p.getAddress().getHostAddress() + ":" + p.getPort());
                // Devuelve el buffer de datos que se utiliza para recibir o enviar
                nombre = new String(p.getData(), 0, p.getLength());
                //String nombre = new String(b);
                System.out.println("Nombre: "+nombre);

                }
                if(i== 1)
                {
                // Creado para recibir paquetes de longitud lenght
                p = new DatagramPacket(new byte[200], 200);
                s.receive(p); // Recibe el DatagramPacket p
                // Devuelve el buffer de datos que se utiliza para recibir o enviar
                apellidoPaterno = new String(p.getData(), 0, p.getLength());
                //String nombre = new String(b);
                System.out.println("Apellido paterno: "+apellidoPaterno);

                }
                if(i== 2)
                {
                // Creado para recibir paquetes de longitud lenght
                p = new DatagramPacket(new byte[200], 200);
                s.receive(p); // Recibe el DatagramPacket p
                // Agrega la dirección y el puerto de donde es enviado p
                 // Devuelve el buffer de datos que se utiliza para recibir o enviar
                apellidoMaterno = new String(p.getData(), 0, p.getLength());
                //String nombre = new String(b);
                System.out.println("Apelldio materno: "+apellidoMaterno);

                }
                if(i== 3)
                {
                // Creado para recibir paquetes de longitud lenght
                p = new DatagramPacket(new byte[200], 200);
                s.receive(p); // Recibe el DatagramPacket p
                // Agrega la dirección y el puerto de donde es enviado p
                 // Devuelve el buffer de datos que se utiliza para recibir o enviar
                fecha = new String(p.getData(), 0, p.getLength());
                //String nombre = new String(b);
                System.out.println("Fecha de nacimiento: "+ fecha);                   
                }
                if(i== 4)
                {// 14/05/1996
                String mensaje = primerosCuatroCaracteresRFC(nombre, apellidoPaterno, apellidoMaterno)+fecha.substring(8,10)+fecha.substring(3,5)+fecha.substring(0,2);
                byte[] b = mensaje.getBytes();
                DatagramPacket r = new DatagramPacket(b, b.length, InetAddress.getByName(p.getAddress().getHostAddress()),
                        p.getPort());
                s.send(r);
                }    
                  
            }
            s.close();;          
        } catch (Exception e) {
            e.printStackTrace();
        } // catch
    }
}