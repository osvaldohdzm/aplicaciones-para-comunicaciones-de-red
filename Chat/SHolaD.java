import java.net.*;
import java.io.*;

public class SHolaD {
    public static void main(String[] args) {
        try {
            // Crea un socket de datagrama y asigna puerta a la computadora local
            DatagramSocket s = new DatagramSocket(2000);
            // Espera el mensaje del cliente
            System.out.println("Servidor iniciado, esperando cliente");
            while (true) {
                // Creado para recibir paquetes de longitud lenght
                DatagramPacket p = new DatagramPacket(new byte[200], 200);
                s.receive(p); // Recibe el DatagramPacket p
                // Agrega la dirección y el puerto de donde es enviado p
                System.out.println("Datagrama recibido desde " + p.getAddress().getHostAddress() + ":" + p.getPort());
                // Devuelve el buffer de datos que se utiliza para recibir o enviar
                String msj = new String(p.getData(), 0, p.getLength());
                System.out.println("Con el mensaje: " + msj);
                System.out.println("Escribe un mensaje para responder al cliente");
                // Lee el texto de secuencia de entrada
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                String mensaje = br.readLine();
                // Devuelve la cadena de bytes
                byte[] b = mensaje.getBytes();
                DatagramPacket r = new DatagramPacket(b, b.length, InetAddress.getByName(p.getAddress().getHostAddress()),
                        p.getPort());
                s.send(r);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } // catch
    }
}