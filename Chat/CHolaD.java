import java.net.*;
import java.io.*;

public class CHolaD {
    public static void main(String[] args){
        try{
            System.out.println("Cliente iniciado");
            DatagramSocket cl = new DatagramSocket(); //Crea el soceket de datagrama
            while(true) {
                System.out.println("Escriba un mensaje hacia el server:");
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); //Lee el texto de secuencia de entrada
                String mensaje = br.readLine();
                byte[] b = mensaje.getBytes(); //Devuelve la cadena de bytes 
                String dst = "127.0.0.1";
                int pto = 2000;
                DatagramPacket p = new DatagramPacket(b,b.length,InetAddress.getByName(dst),pto);
                cl.send(p);
                // Creado para recibir paquetes de longitud lenght
                DatagramPacket r = new DatagramPacket(new byte[2000], 2000);
                cl.receive(r); // Recibe el DatagramPacket r
                // Agrega la dirección y el puerto de donde es enviado r
                System.out.println("Datagrama recibido desde " + r.getAddress().getHostAddress() + ":" + r.getPort());
                // Devuelve el buffer de datos que se utiliza para recibir o enviar
                String msj = new String(r.getData(), 0, r.getLength());
                System.out.println("Con el mensaje: " + msj);
            }
        }catch(Exception e){
            e.printStackTrace();
        }//catch
    }
}